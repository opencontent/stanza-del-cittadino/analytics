from typing import List
from fastapi import Query


class BookingStatParams:
    def __init__(
        self,
        filter: str = Query(
            default='service', description="Filtro di aggregazione del numero di prenotazioni (service o calendar)"
        ),
        filter_id: List[str] = Query(default=None, description="Identificativo del servizio o del calendario"),
        interval: str = Query(
            default='daily', description="Livello di aggregazione delle prenotazioni (daily, monthly o yearly)"
        ),
        start_date: str = Query(
            default=None,
            description="Data d'inizio (standard ISO8601) a partire dalla quale visualizzare le statistiche",
        ),
        end_date: str = Query(
            default=None, description="Data di fine (standard ISO8601) entro la quale visualizzare le statistiche"
        ),
    ):
        self.interval = interval
        self.filter = filter
        self.filter_id = filter_id
        self.start_date = start_date
        self.end_date = end_date


class ServiceStatParams:
    def __init__(
        self,
        filter_id: List[str] = Query(default=None, description="Identificativo del servizio o del calendario"),
        interval: str = Query(
            default='daily', description="Livello di aggregazione delle prenotazioni (daily, monthly o yearly)"
        ),
        start_date: str = Query(
            default=None,
            description="Data d'inizio (standard ISO8601) a partire dalla quale visualizzare le statistiche",
        ),
        end_date: str = Query(
            default=None, description="Data di fine (standard ISO8601) entro la quale visualizzare le statistiche"
        ),
    ):
        self.interval = interval
        self.filter_id = filter_id
        self.start_date = start_date
        self.end_date = end_date
