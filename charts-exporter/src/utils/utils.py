from datetime import datetime, timedelta
import calendar


def get_days_difference(start_date, end_date):
    return (datetime.strptime(end_date, "%Y-%m-%d") - datetime.strptime(start_date, "%Y-%m-%d")).days


def get_end_date_from_calendar_week(year, calendar_week):
    monday = datetime.strptime(f'{year}-{calendar_week}-1', "%G-%V-%u").date()
    return monday + timedelta(days=6.9)


def set_start_and_end_date(start_date, end_date):
    if not start_date and not end_date:
        start_date = datetime.today().replace(day=1).strftime("%Y-%m-%d")
        end_date = datetime.today().strftime("%Y-%m-%d")
    elif start_date and not end_date:
        date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = date.replace(day=calendar.monthrange(date.year, date.month)[1]).strftime("%Y-%m-%d")
    elif not start_date and end_date:
        start_date = datetime.strptime(end_date, "%Y-%m-%d").replace(day=1).strftime("%Y-%m-%d")
    return start_date, end_date


def get_list_of_ids(aggregation_param, aggregation_param_id):
    list_of_ids = ''
    for i in range(len(aggregation_param_id)):
        if i < len(aggregation_param_id) - 1:
            list_of_ids = list_of_ids + aggregation_param + "=\'" + aggregation_param_id[i] + "\' OR "
        else:
            list_of_ids = list_of_ids + aggregation_param + "=\'" + aggregation_param_id[i]
    return list_of_ids


def get_daily_params(start_date, end_date):
    days_diff = get_days_difference(start_date, end_date)
    start_date = (datetime.strptime(start_date, "%Y-%m-%d") - timedelta(days=days_diff)).strftime("%Y-%m-%d")
    end_date = (datetime.strptime(end_date, "%Y-%m-%d") - timedelta(days=days_diff)).strftime("%Y-%m-%d")
    return start_date, end_date


def get_params_as_dict(tenant_id, aggregation_param_id, start_date, end_date):
    return {
        'tenant_id': tenant_id,
        'aggregation_param_id': aggregation_param_id,
        'start_date': start_date,
        'end_date': end_date,
    }
