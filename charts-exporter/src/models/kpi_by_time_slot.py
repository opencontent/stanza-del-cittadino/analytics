from enum import Enum
from typing import List
from datetime import time
from pydantic import BaseModel


class TimeSlots(Enum):
    EIGHT_TWELVE = "08:00-12:00"
    TWELVE_SIXTEEN = "12:00-16:00"
    SIXTEEN_TWENTY = "16:00-20:00"
    TWENTY_MIDNIGHT = "20:00-24:00"
    MIDNIGHT_FOUR = "24:00-04:00"
    FOUR_EIGHT = "04:00-08:00"


class TimeSlotsRange(Enum):
    EIGHT_TWELVE = [8, 12]
    TWELVE_SIXTEEN = [12, 16]
    SIXTEEN_TWENTY = [16, 20]
    TWENTY_MIDNIGHT = [20, 0]
    MIDNIGHT_FOUR = [0, 4]
    FOUR_EIGHT = [4, 8]


class DataByTimeSlot(BaseModel):
    data: List[int]
    label: str


class KpiByTimeSlot:
    def __init__(self, labels):
        self.labels = [slot.value for slot in TimeSlots]
        self.datasets = [DataByTimeSlot(data=[0] * 6, label=label) for label in labels]

    def set_stats(self, data):
        hours = []
        values = []
        for el in data:
            hours.append([idx[0] for idx in el])
            values.append([value[1] for value in el])
        for i in range(len(self.datasets)):
            for j in range(len(hours[i])):
                for k in range(len(list(TimeSlotsRange))):
                    timeslotrange = list(TimeSlotsRange)[k].value
                    start_time = time(timeslotrange[0])
                    end_time = time(timeslotrange[1]) if timeslotrange[1] != 0 else time(23, 59, 59)
                    if start_time <= time(hours[i][j]) < end_time:
                        self.datasets[i].data[k] += values[i][j]


class KpiByTimeSlotModel(BaseModel):
    labels: List[TimeSlots] = [slot.value for slot in TimeSlots]
    datasets: List[DataByTimeSlot]
