from .kpi import Kpi


class BookingsNumber(Kpi):
    def __init__(self, no_of_bookings):
        super().__init__(no_of_bookings)
