from enum import Enum
from typing import List

from pydantic import BaseModel


class WeekDays(Enum):
    MONDAY = "Lunedì"
    TUESDAY = "Martedì"
    WEDNESDAY = "Mercoledì"
    THURSDAY = "Giovedì"
    FRIDAY = "Venerdì"
    SATURDAY = "Sabato"
    SUNDAY = "Domenica"


class DataByWeekDay(BaseModel):
    data: List[int]
    label: str


class KpiByDayOfWeek:
    def __init__(self, labels):
        self.labels = [day.value for day in WeekDays]
        self.datasets = [DataByWeekDay(data=[0] * 7, label=label) for label in labels]

    def set_stats(self, data):
        indices = []
        values = []
        for el in data:
            indices.append([idx[0] for idx in el])
            values.append([value[1] for value in el])
        for i in range(len(self.datasets)):
            for j in range(len(self.datasets[i].data)):
                if j + 1 in indices[i]:
                    self.datasets[i].data[j] = values[i][indices[i].index(j + 1)]


class KpiByDayOfWeekModel(BaseModel):
    labels: List[WeekDays] = [day.value for day in WeekDays]
    datasets: List[DataByWeekDay]
