from .kpi import Kpi


class ActiveUsersNumber(Kpi):
    def __init__(self, active_users_count):
        super().__init__(active_users_count)
