from .kpi import Kpi


class ApplicationsNumber(Kpi):
    def __init__(self, applications_count):
        super().__init__(applications_count)
