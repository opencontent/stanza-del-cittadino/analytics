from datetime import datetime
from typing import Optional, Union

from dateutil.relativedelta import relativedelta

from src.utils.utils import get_days_difference

from pydantic import BaseModel


class Kpi:
    def __init__(self, kpi):
        self.kpi = kpi
        self.previous_period_var = 0
        self.avg_var = 0

    def set_stats(self, kpi_previous_period, kpi_avg, start_date, end_date):
        self.kpi = [[0]] if not self.kpi else self.kpi
        kpi_previous = [[0]] if not kpi_previous_period else kpi_previous_period
        kpi_avg = [[0]] if not kpi_avg or kpi_avg[0][0] is None else kpi_avg
        self.kpi = sum(self.kpi[i][0] for i in range(len(self.kpi)) if self.kpi[i][0] is not None)
        self.set_previous_period_var(
            sum(kpi_previous[i][0] for i in range(len(kpi_previous)) if kpi_previous[i][0] is not None)
        )
        self.set_avg_var(
            kpi_avg[0][0]
            / get_days_difference(
                (datetime.strptime(end_date, "%Y-%m-%d") - relativedelta(months=3)).strftime("%Y-%m-%d"), end_date
            )
        )

    def set_previous_period_var(self, kpi_previous_period):
        if kpi_previous_period < self.kpi:
            if kpi_previous_period != 0:
                self.previous_period_var = round((abs(kpi_previous_period - self.kpi)) / kpi_previous_period, 2) * 100
            else:
                self.previous_period_var = self.kpi * 100
        elif kpi_previous_period > self.kpi:
            if self.kpi != 0:
                self.previous_period_var = -round((abs(kpi_previous_period - self.kpi)) / kpi_previous_period, 2) * 100
            else:
                self.previous_period_var = -kpi_previous_period * 100
        else:
            self.previous_period_var = 0

    def set_avg_var(self, kpi_avg):
        if kpi_avg < self.kpi:
            if kpi_avg != 0:
                self.avg_var = round((abs(kpi_avg - self.kpi)) / kpi_avg, 2) * 100
            else:
                self.avg_var = self.kpi * 100
        elif kpi_avg > self.kpi:
            if self.kpi != 0:
                self.avg_var = -round((abs(kpi_avg - self.kpi)) / kpi_avg, 2) * 100
            else:
                self.avg_var = -kpi_avg * 100
        else:
            self.avg_var = 0

    def to_dict(self, kpi, previous_period_var, avg_var):
        return {kpi: self.kpi, previous_period_var: self.previous_period_var, avg_var: self.avg_var}


class KpiModel(BaseModel):
    kpi: Union[int, float, None]
    previous_period_var: Optional[float]
    avg_var: Optional[float]
