from .kpi import Kpi


class ApplicationsByUser(Kpi):
    def __init__(self, avg_applications_by_user):
        super().__init__(avg_applications_by_user)
