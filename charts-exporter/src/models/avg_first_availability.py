from .kpi import Kpi


class AvgFirstAvailability(Kpi):
    def __init__(self, first_availability):
        super().__init__(first_availability)

    def set_stats(self, kpi_previous, kpi_avg, start_date=None, end_date=None):
        self.kpi = [[None]] if not self.kpi else self.kpi
        kpi_previous = [[None]] if not kpi_previous else kpi_previous
        kpi_avg = [[None]] if not kpi_avg else kpi_avg

        if self.kpi[0][0] is not None and kpi_previous[0][0] is not None and kpi_avg[0][0] is not None:
            self.kpi = sum(self.kpi[i][0] for i in range(len(self.kpi)) if self.kpi[i][0] is not None) / len(self.kpi)
            kpi_previous = sum(
                kpi_previous[i][0] for i in range(len(kpi_previous)) if kpi_previous[i][0] is not None
            ) / len(kpi_previous)
            self.set_previous_period_var(kpi_previous)
            self.set_avg_var(kpi_avg[0][0])
        elif self.kpi[0][0] is not None and kpi_previous[0][0] is None and kpi_avg[0][0] is not None:
            self.kpi = sum(self.kpi[i][0] for i in range(len(self.kpi)) if self.kpi[i][0] is not None) / len(self.kpi)
            self.previous_period_var = None
            self.set_avg_var(kpi_avg[0][0])
        elif self.kpi[0][0] is not None and kpi_previous[0][0] is not None and kpi_avg[0][0] is None:
            self.kpi = sum(self.kpi[i][0] for i in range(len(self.kpi)) if self.kpi[i][0] is not None) / len(self.kpi)
            kpi_previous = sum(
                kpi_previous[i][0] for i in range(len(kpi_previous)) if kpi_previous[i][0] is not None
            ) / len(kpi_previous)
            self.set_previous_period_var(kpi_previous)
            self.avg_var = None
        elif self.kpi[0][0] is not None and kpi_previous[0][0] is None and kpi_avg[0][0] is None:
            self.kpi = self.kpi[0][0]
            self.previous_period_var = None
            self.avg_var = None
        else:
            self.kpi = None
            self.previous_period_var = None
            self.avg_var = None
