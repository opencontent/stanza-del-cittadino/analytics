from datetime import datetime
from clickhouse_driver import Client
from dateutil.relativedelta import relativedelta
from src.utils.utils import get_daily_params, get_params_as_dict
from src.utils.logger import clickhouse_logger as logger


class ClickHouseClient:
    def __init__(self, host, user, secret, db):
        self._host = host
        self._user = user
        self._secret = secret
        self._db = db

    def __enter__(self):
        self.client = Client(
            self._host,
            # user=self._user,
            # password=self._secret,
            # secure=True,
            # verify=False,
            database=self._db
            # compression=True
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.client.disconnect()
        except Exception as e:
            logger.exception(f"An error occurred: {e}")

    def get_kpi(
        self,
        tenant_id,
        aggregation_param_id,
        start_date,
        end_date,
        period,
        table_name,
        aggregation_param,
        kpi,
        func_kpi,
        func_kpi_avg,
    ):
        try:
            query = (
                f"SELECT {func_kpi}({kpi})\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
            )
            data = get_params_as_dict(tenant_id, aggregation_param_id, start_date, end_date)
            logger.debug(query)

            start_date_previous, end_date_previous = get_daily_params(start_date, end_date)
            query_previous = (
                f"SELECT {func_kpi}({kpi})\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
            )
            data_previous = get_params_as_dict(tenant_id, aggregation_param_id, start_date_previous, end_date_previous)
            logger.debug(query_previous)

            start_date_avg = (datetime.strptime(end_date, "%Y-%m-%d") - relativedelta(months=3)).strftime("%Y-%m-%d")
            query_avg = (
                f"SELECT {func_kpi_avg}({kpi})\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s)"
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
            )
            data_avg = get_params_as_dict(tenant_id, aggregation_param_id, start_date_avg, end_date)
            logger.debug(query_avg)
            print(data_avg)

            result = self.client.execute(query, data)
            result_previous = self.client.execute(query_previous, data_previous)
            result_avg = self.client.execute(query_avg, data_avg)
            return result, result_previous, result_avg
        except Exception as e:
            logger.error(f"An error occurred: {e}")

    def get_kpi_by_day_of_week(
        self, tenant_id, aggregation_param_id, start_date, end_date, table_name, aggregation_param, kpi, func_kpi
    ):
        try:
            query = (
                f"SELECT toDayOfWeek(day), {func_kpi}({kpi})\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
                f"GROUP BY toDayOfWeek(day)"
            )
            data = get_params_as_dict(tenant_id, aggregation_param_id, start_date, end_date)
            logger.debug(query)
            result = self.client.execute(query, data)
            return result
        except Exception as e:
            logger.error(f"An error occurred: {e}")

    def get_kpi_by_time_slot(
        self, tenant_id, aggregation_param_id, start_date, end_date, table_name, aggregation_param, kpi, func_kpi
    ):
        try:
            query = (
                f"SELECT multiIf("
                f"has(range(8, 12), toHour(day)), 8,"
                f"has(range(12, 16), toHour(day)), 12,"
                f"has(range(16, 20), toHour(day)), 16,"
                f"has(range(20, 24), toHour(day)), 20,"
                f"has(range(0, 4), toHour(day)), 0,"
                f"has(range(4, 8), toHour(day)), 4,"
                f"null) as hour, {func_kpi}({kpi})\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
                f"GROUP BY hour"
            )
            data = get_params_as_dict(tenant_id, aggregation_param_id, start_date, end_date)
            logger.debug(query)
            result = self.client.execute(query, data)
            return result
        except Exception as e:
            logger.error(f"An error occurred: {e}")

    def get_applications_by_user(
        self,
        tenant_id,
        aggregation_param_id,
        start_date,
        end_date,
        period,
        table_name,
        aggregation_param,
        kpi,
        func_kpi,
        func_kpi_avg,
    ):
        try:
            query = (
                f"SELECT {func_kpi}({kpi})\n"
                f"FROM ("
                f"SELECT SUM(applications_count) as {kpi}\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
                f"GROUP BY user_id"
                f")"
            )
            data = get_params_as_dict(tenant_id, aggregation_param_id, start_date, end_date)
            logger.debug(query)

            start_date_previous, end_date_previous = get_daily_params(start_date, end_date)
            query_previous = (
                f"SELECT {func_kpi}({kpi})\n"
                f"FROM ("
                f"SELECT SUM(applications_count) as {kpi}\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
                f"GROUP BY user_id"
                f")"
            )
            data_previous = get_params_as_dict(tenant_id, aggregation_param_id, start_date_previous, end_date_previous)
            logger.debug(query_previous)

            start_date_avg = (datetime.strptime(end_date, "%Y-%m-%d") - relativedelta(months=3)).strftime("%Y-%m-%d")
            query_avg = (
                f"SELECT {func_kpi_avg}({kpi})\n"
                f"FROM ("
                f"SELECT SUM(applications_count) as {kpi}\n"
                f"FROM {table_name}\n"
                f"WHERE tenant_id=%(tenant_id)s "
                f"AND ({aggregation_param} IN %(aggregation_param_id)s) "
                f"AND (day>=%(start_date)s AND day<%(end_date)s)\n"
                f"GROUP BY user_id"
                f")"
            )
            data_avg = get_params_as_dict(tenant_id, aggregation_param_id, start_date_avg, end_date)
            logger.debug(query_avg)

            result = self.client.execute(query, data)
            result_previous = self.client.execute(query_previous, data_previous)
            result_avg = self.client.execute(query_avg, data_avg)
            return result, result_previous, result_avg
        except Exception as e:
            logger.error(f"An error occurred: {e}")
