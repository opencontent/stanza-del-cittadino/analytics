from fastapi import FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from starlette_exporter import PrometheusMiddleware, handle_metrics
from pythonjsonlogger import jsonlogger

from utils.logger import app_logger as logger
from routes.services import services
from routes.bookings import bookings

import os
import uvicorn
import logging
import locale

app = FastAPI()

PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='analytics_charts_exporter')

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)
app.add_middleware(CORSMiddleware, allow_origins='*', allow_credentials=True, allow_methods=["*"], allow_headers=["*"])


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)


class Status(BaseModel):
    status: str


@app.on_event("startup")
def startup_event():
    logging.info('Initializing API ...')
    uvicorn_logger = logging.getLogger('uvicorn.access')
    log_handler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    log_handler.setFormatter(formatter)
    uvicorn_logger.addHandler(log_handler)


@app.on_event("shutdown")
def shutdown_event():
    logger.info("Shutting down...")


@app.get('/status', response_model=Status, status_code=status.HTTP_200_OK, tags=["Status"])
def check_service_status():
    return Status(status='Everything OK!').dict()


app.include_router(bookings, tags=["Bookings"], prefix="/stats/{tenant_id}/bookings")
app.include_router(services, tags=["Services"], prefix="/stats/{tenant_id}/services")
app.add_route("/metrics", handle_metrics)

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', proxy_headers=True, forwarded_allow_ips='*', access_log=False)
