import os

from fastapi import APIRouter, Depends

from src.api.api_docs import ServiceStatParams
from src.controllers.clickhouse_client import ClickHouseClient
from src.models.active_users_number import ActiveUsersNumber
from src.models.applications_by_user import ApplicationsByUser
from src.models.applications_number import ApplicationsNumber
from src.models.kpi import KpiModel
from src.models.kpi_by_day_of_week import KpiByDayOfWeekModel, KpiByDayOfWeek, DataByWeekDay
from src.models.kpi_by_time_slot import KpiByTimeSlotModel, DataByTimeSlot, KpiByTimeSlot
from src.utils.utils import set_start_and_end_date
from src.utils.logger import services_logger as logger

CLICKHOUSE_HOST = os.getenv('CLICKHOUSE_HOST', default='localhost')
CLICKHOUSE_USER = os.getenv('CLICKHOUSE_USER', default='')
CLICKHOUSE_SECRET = os.getenv('CLICKHOUSE_SECRET', default='')
CLICKHOUSE_DB = os.getenv('CLICKHOUSE_DB', default='sdc_analytics')

services = APIRouter()


@services.get('/applications', response_model=KpiModel)
def get_applications_count_by_service(tenant_id: str, params: ServiceStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            applications_count, applications_count_previous, applications_count_avg = client.get_kpi(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'daily',
                'service_stats',
                'service_id',
                'applications_count',
                'SUM',
                'SUM',
            )

        logger.debug(
            f"Applications: {applications_count}, Previous period: {applications_count_previous}, "
            f"Average: {applications_count_avg}"
        )

        applications_count_stat = ApplicationsNumber(applications_count)
        applications_count_stat.set_stats(
            applications_count_previous, applications_count_avg, params.start_date, params.end_date
        )

        return vars(applications_count_stat)
    else:
        return KpiModel(kpi=0, previous_period_var=0, avg_var=0).dict()


@services.get('/active-users', response_model=KpiModel)
def get_active_users_count_by_service(tenant_id: str, params: ServiceStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            active_users_count, active_users_count_previous, active_users_count_avg = client.get_kpi(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'daily',
                'service_stats',
                'service_id',
                'distinct user_id',
                'COUNT',
                'COUNT',
            )

        logger.debug(
            f"Active Users: {active_users_count}, Previous period: {active_users_count_previous}, "
            f"Average: {active_users_count_avg}"
        )

        active_users_count_stat = ActiveUsersNumber(active_users_count)
        active_users_count_stat.set_stats(
            active_users_count_previous, active_users_count_avg, params.start_date, params.end_date
        )

        return vars(active_users_count_stat)
    else:
        return KpiModel(kpi=0, previous_period_var=0, avg_var=0).dict()


@services.get('/applications-by-user', response_model=KpiModel)
def get_applications_by_user(tenant_id: str, params: ServiceStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            (
                applications_by_user,
                applications_by_user_previous,
                applications_by_user_avg,
            ) = client.get_applications_by_user(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'daily',
                'service_stats',
                'service_id',
                'applications_by_user',
                'AVG',
                'SUM',
            )

        logger.debug(
            f"Applications by user: {applications_by_user}, Previous period: {applications_by_user_previous}, "
            f"Average: {applications_by_user_avg}"
        )

        applications_by_user_stat = ApplicationsByUser(applications_by_user)
        applications_by_user_stat.set_stats(
            applications_by_user_previous, applications_by_user_avg, params.start_date, params.end_date
        )

        return vars(applications_by_user_stat)
    else:
        return KpiModel(kpi=0, previous_period_var=0, avg_var=0).dict()


@services.get('/applications-and-users-by-weekday', response_model=KpiByDayOfWeekModel)
def get_applications_and_users_count_by_day_of_week(tenant_id: str, params: ServiceStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            applications_by_day_of_week = client.get_kpi_by_day_of_week(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'service_stats',
                'service_id',
                'applications_count',
                'SUM',
            )
            active_users_by_day_of_week = client.get_kpi_by_day_of_week(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'service_stats',
                'service_id',
                'distinct user_id',
                'COUNT',
            )
        logger.debug(f"Applications: {applications_by_day_of_week} Users: {active_users_by_day_of_week}")

        applications_and_users_by_day_of_week = KpiByDayOfWeek(['Utenti', 'Pratiche'])
        applications_and_users_by_day_of_week.set_stats([active_users_by_day_of_week, applications_by_day_of_week])

        return vars(applications_and_users_by_day_of_week)
    else:
        return KpiByDayOfWeekModel(
            datasets=[DataByWeekDay(data=[0] * 7, label="Utenti"), DataByWeekDay(data=[0] * 7, label="Pratiche")]
        ).dict()


@services.get('/applications-and-users-by-time-slot', response_model=KpiByTimeSlotModel)
def get_applications_and_users_count_by_time_slot(tenant_id: str, params: ServiceStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            applications_by_time_slot = client.get_kpi_by_time_slot(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'service_stats',
                'service_id',
                'applications_count',
                'SUM',
            )
            active_users_by_time_slot = client.get_kpi_by_time_slot(
                tenant_id,
                params.filter_id,
                params.start_date,
                params.end_date,
                'service_stats',
                'service_id',
                'distinct user_id',
                'COUNT',
            )
        logger.debug(f"Applications: {applications_by_time_slot} Users: {active_users_by_time_slot}")

        applications_and_users_by_time_slot = KpiByTimeSlot(['Utenti', 'Pratiche'])
        applications_and_users_by_time_slot.set_stats([active_users_by_time_slot, applications_by_time_slot])

        return vars(applications_and_users_by_time_slot)
    else:
        return KpiByTimeSlotModel(
            datasets=[DataByTimeSlot(data=[0] * 6, label="Utenti"), DataByTimeSlot(data=[0] * 6, label="Pratiche")]
        ).dict()
