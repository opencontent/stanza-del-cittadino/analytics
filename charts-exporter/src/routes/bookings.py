import os

from fastapi import APIRouter, Depends

from src.api.api_docs import BookingStatParams
from src.controllers.clickhouse_client import ClickHouseClient
from src.models.avg_first_availability import AvgFirstAvailability
from src.models.bookings_number import BookingsNumber
from src.models.kpi import KpiModel
from src.utils.utils import set_start_and_end_date
from src.utils.logger import bookings_logger as logger

CLICKHOUSE_HOST = os.getenv('CLICKHOUSE_HOST', default='localhost')
CLICKHOUSE_USER = os.getenv('CLICKHOUSE_USER', default='')
CLICKHOUSE_SECRET = os.getenv('CLICKHOUSE_SECRET', default='')
CLICKHOUSE_DB = os.getenv('CLICKHOUSE_DB', default='sdc_analytics')

bookings = APIRouter()


@bookings.get('', response_model=KpiModel)
def get_no_of_bookings(tenant_id: str, params: BookingStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            if params.filter == 'service':
                no_of_bookings, no_of_bookings_previous, no_of_bookings_avg = client.get_kpi(
                    tenant_id,
                    params.filter_id,
                    params.start_date,
                    params.end_date,
                    'daily',
                    'calendars_daily_aggregation_by_service_v2',
                    'service_id',
                    'no_of_bookings',
                    'SUM',
                    'SUM',
                )
            elif params.filter == 'calendar':
                no_of_bookings, no_of_bookings_previous, no_of_bookings_avg = client.get_kpi(
                    tenant_id,
                    params.filter_id,
                    params.start_date,
                    params.end_date,
                    'daily',
                    'calendars_daily_aggregation_by_calendar_v2',
                    'calendar_id',
                    'no_of_bookings',
                    'SUM',
                    'SUM',
                )

        logger.debug(
            f"Bookings: {no_of_bookings}, Previous period: {no_of_bookings_previous}, "
            f"Average: {no_of_bookings_avg}"
        )

        bookings_stat = BookingsNumber(no_of_bookings)
        bookings_stat.set_stats(no_of_bookings_previous, no_of_bookings_avg, params.start_date, params.end_date)

        return vars(bookings_stat)
    else:
        return KpiModel(kpi=0, previous_period_var=0, avg_var=0).dict()


@bookings.get('/availability', response_model=KpiModel)
def get_first_availability(tenant_id: str, params: BookingStatParams = Depends()):
    if params.filter_id:
        params.start_date, params.end_date = set_start_and_end_date(params.start_date, params.end_date)
        with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            if params.filter == 'service':
                first_availability, first_availability_previous, first_availability_avg = client.get_kpi(
                    tenant_id,
                    params.filter_id,
                    params.start_date,
                    params.end_date,
                    'daily',
                    'calendars_daily_aggregation_by_service_v2',
                    'service_id',
                    'first_availability',
                    'AVG',
                    'AVG',
                )
            elif params.filter == 'calendar':
                first_availability, first_availability_previous, first_availability_avg = client.get_kpi(
                    tenant_id,
                    params.filter_id,
                    params.start_date,
                    params.end_date,
                    'daily',
                    'calendars_daily_aggregation_by_calendar_v2',
                    'calendar_id',
                    'first_availability',
                    'AVG',
                    'AVG',
                )

        logger.debug(
            f"First Availability: {first_availability}, Previous period: {first_availability_previous}, "
            f"Average: {first_availability_avg}"
        )

        first_availability_stat = AvgFirstAvailability(first_availability)
        first_availability_stat.set_stats(first_availability_previous, first_availability_avg)

        return vars(first_availability_stat)
    else:
        return KpiModel(kpi=None, previous_period_var=None, avg_var=None).dict()
