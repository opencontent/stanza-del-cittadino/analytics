#!/bin/bash
[[ $DEBUG ]] && set -x

readonly host=${KSQLDB_HOST:-localhost:8088}
readonly partitions=${KAFKA_DEFAULT_TOPIC_PARTITIONS:-1}
#echo 'Aspetto che Kafka sia online'

echo "Ksqldb server host: ${host}"

echo "Kafka default partitions: ${partitions}"

wait-for-it "${host}/ksql"

if [[ $DROP_IF_EXISTS == 'true' ]]; then
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $'{ "ksql": "DROP TABLE IF EXISTS applications_by_day_tenant_service_and_user;", "streamsProperties": {} }'
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $'{ "ksql": "DROP TABLE IF EXISTS applications_by_id;", "streamsProperties": {} }'
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $'{ "ksql": "DROP STREAM IF EXISTS applications_stream;", "streamsProperties": {} }'
fi

curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $"{ \"ksql\": \"SET 'auto.offset.reset' = 'earliest';\", \"streamsProperties\": {} }"
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $"{ \"ksql\": \"CREATE STREAM IF NOT EXISTS applications_stream (id VARCHAR, tenant VARCHAR, service_id VARCHAR, user VARCHAR, status_name VARCHAR, created_at VARCHAR) WITH (KAFKA_TOPIC='applications', PARTITIONS=${partitions}, KEY_FORMAT='KAFKA', VALUE_FORMAT='JSON');\", \"streamsProperties\": {} }"
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $"{ \"ksql\": \"CREATE TABLE IF NOT EXISTS applications_by_id WITH (FORMAT='JSON') AS SELECT id, LATEST_BY_OFFSET(tenant) AS tenant_id, LATEST_BY_OFFSET(service_id) AS service_id, LATEST_BY_OFFSET(user) AS user_id, CONVERT_TZ(PARSE_TIMESTAMP(LATEST_BY_OFFSET(created_at), 'yyyy-MM-dd''T''HH:mm:ssXXX'), 'UTC', 'Europe/Rome') as day FROM applications_stream GROUP BY id EMIT CHANGES;\", \"streamsProperties\": {} }"
curl -X "POST" "http://${host}/ksql" -H "Accept: application/vnd.ksql.v1+json" -d $"{ \"ksql\": \"CREATE TABLE IF NOT EXISTS applications_by_day_tenant_service_and_user AS SELECT day, tenant_id, service_id, user_id, count(id) as applications_count FROM applications_by_id GROUP BY day, tenant_id, service_id, user_id EMIT CHANGES;\", \"streamsProperties\": {} }"
