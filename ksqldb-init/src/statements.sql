-- Stream
CREATE STREAM IF NOT EXISTS applications_stream (
    id VARCHAR,
    tenant VARCHAR,
    service_id VARCHAR,
    user VARCHAR,
    status_name VARCHAR,
    created_at VARCHAR,
) WITH (
    KAFKA_TOPIC='applications',
    PARTITIONS=20,
    KEY_FORMAT='KAFKA',
    VALUE_FORMAT='JSON'
);

-- Tables
CREATE TABLE applications_by_id WITH (FORMAT='JSON') AS
SELECT id,
       LATEST_BY_OFFSET(tenant) AS tenant_id,
       LATEST_BY_OFFSET(service_id) AS service_id,
       LATEST_BY_OFFSET(user) AS user_id,
       CONVERT_TZ(PARSE_TIMESTAMP(LATEST_BY_OFFSET(created_at), 'yyyy-MM-dd''T''HH:mm:ssXXX'), 'UTC', 'Europe/Rome') as day
FROM applications_stream
GROUP BY id
EMIT CHANGES;


CREATE TABLE IF NOT EXISTS applications_by_day_tenant_service_and_user AS
SELECT day,
       tenant_id,
       service_id,
       user_id,
       count(id) as applications_count
FROM applications_by_id
GROUP BY day, tenant_id, service_id, user_id
EMIT CHANGES;

DROP TABLE IF EXISTS applications_by_day_tenant_service_and_user;
DROP TABLE IF EXISTS applications_by_id;
DROP STREAM IF EXISTS applications_stream;
