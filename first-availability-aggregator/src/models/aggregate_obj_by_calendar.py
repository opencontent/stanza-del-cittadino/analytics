from .aggregate_obj import AggregateObj

class AggregateObjByCalendar(AggregateObj):
    def __init__(self):
        super().__init__()
        self.calendar_id = None
    
    def get_calendar_id(self):
        return self.calendar_id
    
    def set_calendar_id(self, calendar_id):
        self.calendar_id = calendar_id