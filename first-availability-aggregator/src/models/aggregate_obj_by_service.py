from .aggregate_obj import AggregateObj

class AggregateObjByService(AggregateObj):
    def __init__(self):
        super().__init__()
        self.service_id = None
    
    def get_service_id(self):
        return self.service_id
    
    def set_service_id(self, service_id):
        self.service_id = service_id