from datetime import datetime

class AggregateObj:

    def __init__(self):
        self.day = None
        self.tenant_id = None
        self.no_of_bookings = None
        self.first_availability = None
        self.week = None
        self.month = None
        self.year = None

    def get_day(self):
        return self.day
    
    def set_day(self, day):
        self.day = day

    def get_tenant_id(self):
        return self.tenant_id
    
    def set_tenant_id(self, tenant_id):
        self.tenant_id = tenant_id
    
    def get_no_of_bookings(self):
        return self.first_availability
    
    def set_no_of_bookings(self, no_of_bookings):
        self.no_of_bookings = no_of_bookings
    
    def compute_no_of_bookings(no_of_bookings):
        return no_of_bookings + 1

    def get_first_availability(self):
        return self.first_availability
    
    def set_first_availability(self, first_availability):
        self.first_availability = first_availability
    
    def compute_first_availability(start_date, end_date):
        return (datetime.strptime(end_date, "%Y-%m-%d") - datetime.strptime(start_date.split('T')[0], '%Y-%m-%d')).days

    def get_week(self):
        return self.week
    
    def set_week(self, week):
        self.week = week

    def get_month(self):
        return self.month
    
    def set_month(self, month):
        self.month = month

    def get_year(self):
        return self.year
    
    def set_year(self, year):
        self.year = year