from multiprocessing.sharedctypes import Value
from pydantic import BaseModel, validator
from uuid import UUID
from datetime import datetime, date

class Meeting(BaseModel):
    id: str
    event_version: str
    application_id: str
    status_name: str
    tenant_id: str
    calendar_id: str
    service_id: str
    created_at: str
    first_available_date: str
    first_available_slot: str
    first_availability_updated_at: str

    @validator('event_version')
    def event_version_must_be_two(cls, v):
        if '2' not in v:
            raise ValueError('event must have 2 as version')
        return v.title()
    
    @validator('status_name')
    def status_name_cannot_be_draft(cls, v):
        if 'status_draft' in v:
            raise ValueError('status_name cannot be status_draft')
        return v.title()