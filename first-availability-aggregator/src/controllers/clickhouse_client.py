from aioch import Client
from pythonjsonlogger import jsonlogger
from datetime import datetime

import logging

logger = logging.getLogger(__name__)
logHandler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter()
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)

class ClickHouseClient:
    def __init__(self, host, user, secret, db):
        self._host = host
        self._user = user
        self._secret = secret
        self._db = db
    
    async def __aenter__(self):
        self.client = Client(
            self._host, 
            # user=self._user, 
            # password=self._secret, 
            # secure=True,
            # verify=False,
            database=self._db
            # compression=True
        )
        return self
    
    async def __aexit__(self, exc_type, exc_value, traceback):
        try:
            await self.client.disconnect()
        except Exception:
            logger.exception('An error occurred.')
    
    async def get_most_recent_date(self, table_name, aggregation_param):
        try:
            result = await self.client.execute('SELECT MAX(day) as day, tenant_id, ' + aggregation_param + ' FROM ' + table_name + ' GROUP BY tenant_id, ' + aggregation_param)
            return result
        except Exception:
            logger.exception('An error occurred')

    async def insert(self, aggregate_obj, table_name, aggregation_param):
        try:
            # aggregate_obj[0]['day'] = datetime.strptime(aggregate_obj[0]['day'].split('T')[0], "%Y-%m-%d")
            print(aggregate_obj)
            await self.client.execute('INSERT INTO ' + table_name + ' (day, tenant_id, ' + aggregation_param + ', no_of_bookings, first_availability, week, month, year) VALUES', aggregate_obj)
        except Exception:
            logger.exception('An error occurred.')
    