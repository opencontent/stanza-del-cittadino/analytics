from typing import Set
from fastapi import FastAPI, status
from pydantic import BaseModel, ValidationError
from kafka.structs import TopicPartition
from datetime import datetime, time
from prometheus_client import Counter
from starlette.responses import RedirectResponse
from starlette_exporter import PrometheusMiddleware, handle_metrics
from pythonjsonlogger import jsonlogger
from dotenv import load_dotenv
from sys import exit
from controllers.clickhouse_client import ClickHouseClient
from models.Meeting import Meeting
from models.aggregate_obj import AggregateObj
from models.aggregate_obj_by_calendar import AggregateObjByCalendar
from models.aggregate_obj_by_service import AggregateObjByService

import uvicorn
import aiokafka
import asyncio
import json
import logging
import os
import traceback
import pandas as pd

# instantiate the API
app = FastAPI()

load_dotenv()

# global variables
consumer_task = None
consumer = None

# env variables
KAFKA_TOPIC = os.getenv('KAFKA_TOPIC', default='meetings')
KAFKA_CONSUMER_GROUP_PREFIX = os.getenv('KAFKA_CONSUMER_GROUP_PREFIX',
                                        default='analytics-first-availability-aggregator')
KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS', default='localhost:29092')
PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='analytics_first_availability_aggregator')
CLICKHOUSE_HOST = os.getenv('CLICKHOUSE_HOST', default='localhost')
CLICKHOUSE_USER = os.getenv('CLICKHOUSE_USER', default='')
CLICKHOUSE_SECRET = os.getenv('CLICKHOUSE_SECRET', default='')
CLICKHOUSE_DB = os.getenv('CLICKHOUSE_DB', default='sdc_analytics')

# prometheus counters
REDIRECT_COUNT_SERVICE = Counter("metrics_by_service_total", "Count of bookings by service",
                                 ("no_of_bookings", "first_availability", "tenant", "service"))
REDIRECT_COUNT_CALENDAR = Counter("metrics_availability_by_calendar_total", "Count of bookings by calendar",
                                  ("no_of_bookings", "first_availability", "tenant", "calendar"))

# dataframes for the metrics aggregations
df_aggregate_daily_by_service = pd.DataFrame(
    columns=['day', 'tenant_id', 'service_id', 'no_of_bookings', 'first_availability', 'week', 'month', 'year'])
df_aggregate_daily_by_calendar = pd.DataFrame(
    columns=['day', 'tenant_id', 'calendar_id', 'no_of_bookings', 'first_availability', 'week', 'month', 'year'])

# check if the aggregations has been cleaned once in a day
df_cleaned_up = False

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)

log = logging.getLogger(__name__)


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)


class Status(BaseModel):
    status: str


@app.on_event("startup")
async def startup_event():
    log.info('Initializing API ...')
    logger = logging.getLogger('uvicorn.access')
    log_handler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)

    await initialize()
    await consume()


@app.on_event("shutdown")
async def shutdown_event():
    log.info('Shutting down API')
    consumer_task.cancel()
    await consumer.stop()


@app.get('/status', response_model=Status, status_code=status.HTTP_200_OK)
async def perform_healthcheck():
    await cleanup_df()
    return Status(status='Everything OK!').dict()


async def initialize():
    # loop = asyncio.get_event_loop()
    global consumer
    group_id = f'{KAFKA_CONSUMER_GROUP_PREFIX}'
    log.debug(f'Initializing KafkaConsumer for topic {KAFKA_TOPIC}, group_id {group_id}'
              f' and using bootstrap servers {KAFKA_BOOTSTRAP_SERVERS}')
    consumer = aiokafka.AIOKafkaConsumer(KAFKA_TOPIC,
                                         bootstrap_servers=[broker for broker in KAFKA_BOOTSTRAP_SERVERS.split(',')],
                                         group_id=group_id,
                                         enable_auto_commit=False,
                                         auto_offset_reset='earliest')
    # get cluster layout and join group
    await consumer.start()

    partitions: Set[TopicPartition] = consumer.assignment()
    nr_partitions = len(partitions)
    if nr_partitions != 1:
        log.warning(f'Found {nr_partitions} partitions for topic {KAFKA_TOPIC}. Expecting '
                    f'only one, remaining partitions will be ignored!')
    for tp in partitions:
        # get the log_end_offset
        end_offset_dict = await consumer.end_offsets([tp])
        end_offset = end_offset_dict[tp]

        if end_offset == 0:
            log.warning(f'Topic ({KAFKA_TOPIC}) has no messages (log_end_offset: '
                        f'{end_offset}), skipping initialization ...')
            return

        log.debug(f'Found log_end_offset: {end_offset} seeking to {end_offset - 1}')
        # consumer.seek(tp, end_offset-1)
        await consumer.seek_to_beginning(tp)
        msg = await consumer.getone()
        log.info(f'Initializing API with data from msg: {msg}')

        return


async def consume():
    global consumer_task
    consumer_task = asyncio.create_task(send_consumer_message(consumer))


async def send_consumer_message(consumer):
    try:
        # get the most recent aggregation for each service/calendar saved on db
        most_recent_aggregation_by_service = await get_most_recent_date('calendars_daily_aggregation_by_service_v2',
                                                                        'service_id')
        most_recent_aggregation_by_calendar = await get_most_recent_date('calendars_daily_aggregation_by_calendar_v2',
                                                                         'calendar_id')
        meetings = []
        # consume messages
        async for msg in consumer:
            meeting = json.loads(msg.value)
            try:
                meeting = Meeting(
                    id=meeting['id'],
                    event_version=meeting['event_version'],
                    application_id=meeting['application_id'],
                    status_name=meeting['status_name'],
                    tenant_id=meeting['tenant_id'],
                    calendar_id=meeting['calendar']['id'],
                    service_id=meeting['service_id'],
                    created_at=meeting['created_at'],
                    first_available_date=meeting['first_available_date'],
                    first_available_slot=meeting['first_available_slot'],
                    first_availability_updated_at=meeting['first_availability_updated_at'])

                if meeting.id not in meetings:
                    meetings.append(meeting.id)
                    if not await aggregation_exists(meeting, most_recent_aggregation_by_service, meeting.service_id):
                        await aggregate_daily_by_service(meeting)
                    if not await aggregation_exists(meeting, most_recent_aggregation_by_calendar, meeting.calendar_id):
                        await aggregate_daily_by_calendar(meeting)
            except ValidationError as e:
                print(e)
                pass
            except KeyError:
                traceback.print_exc()
                pass
    except Exception:
        traceback.print_exc()
    finally:
        # will leave consumer group; perform autocommit if enabled
        log.warning('Stopping consumer')
        await consumer.stop()
        exit(1)


# check if the date of the consumed meeting is less recent than the date of the most recent aggregation found on db
async def aggregation_exists(meeting, most_recent_aggregation, aggregation_param_id):
    if most_recent_aggregation is not None:
        if len(most_recent_aggregation) != 0:
            if len([item for item in most_recent_aggregation if
                    item[1] == meeting.tenant_id and item[2] == aggregation_param_id]) != 0:
                created_at = datetime.strptime(meeting.created_at.split('T')[0], "%Y-%m-%d")
                most_recent_aggregation = [item for item in most_recent_aggregation if
                                           datetime.combine(item[0], datetime.min.time()) < created_at and item[
                                               1] == meeting.tenant_id and item[2] == aggregation_param_id]
                if len(most_recent_aggregation) != 0:
                    return False
            else:
                return False
        else:
            return False
        return True
    else:
        return False


async def get_most_recent_date(table_name, aggregation_param):
    async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
        most_recent_date = await client.get_most_recent_date(table_name, aggregation_param)
    return most_recent_date


async def aggregate_daily_by_service(meeting):
    global df_aggregate_daily_by_service

    current_date = meeting.created_at
    if meeting.tenant_id not in df_aggregate_daily_by_service.values:
        aggregate_obj = AggregateObjByService()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_service_id(meeting.service_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObjByService.compute_first_availability(meeting.first_availability_updated_at,
                                                             meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(vars(aggregate_obj), ignore_index=True)
    elif meeting.service_id not in df_aggregate_daily_by_service.values:
        aggregate_obj = AggregateObjByService()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_service_id(meeting.service_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObj.compute_first_availability(meeting.first_availability_updated_at,
                                                    meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(vars(aggregate_obj), ignore_index=True)
    elif (datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(
            df_aggregate_daily_by_service['day'][
                df_aggregate_daily_by_service['service_id'] == meeting.service_id].values[0].split('T')[0],
            '%Y-%m-%d')).days > 0:
        print(df_aggregate_daily_by_service)
        print("\n")
        # send current row to clickhouse
        obj_to_insert = \
            df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == meeting.service_id].to_dict(
                'records')[0]
        aggregate_obj = AggregateObjByService()
        aggregate_obj.set_day(datetime.strptime(obj_to_insert['day'].split('T')[0], "%Y-%m-%d"))
        aggregate_obj.set_tenant_id(obj_to_insert['tenant_id'])
        aggregate_obj.set_service_id(obj_to_insert['service_id'])
        aggregate_obj.set_no_of_bookings(obj_to_insert['no_of_bookings'])
        aggregate_obj.set_first_availability(obj_to_insert['first_availability'] / obj_to_insert['no_of_bookings'])
        aggregate_obj.set_week(obj_to_insert['week'])
        aggregate_obj.set_month(obj_to_insert['month'])
        aggregate_obj.set_year(obj_to_insert['year'])
        obj_to_insert = aggregate_obj

        async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            await client.insert([vars(obj_to_insert)], 'calendars_daily_aggregation_by_service_v2', 'service_id')

        # TODO: export aggregation to /metrics for prometheus
        # await export_metrics(json.loads(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == meeting.service_id].to_json(orient='records'))[0], 'service')

        # delete it from dataframe 
        df_aggregate_daily_by_service.drop(
            df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == meeting.service_id].index,
            inplace=True)

        # add row for the new aggregation
        aggregate_obj = AggregateObjByService()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_service_id(meeting.service_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObjByService.compute_first_availability(meeting.first_availability_updated_at,
                                                             meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(vars(aggregate_obj), ignore_index=True)
    else:
        df_aggregate_daily_by_service.loc[
            df_aggregate_daily_by_service['service_id'] == meeting.service_id, 'no_of_bookings'] += 1
        df_aggregate_daily_by_service.loc[
            df_aggregate_daily_by_service['service_id'] == meeting.service_id, 'first_availability'] += (
            AggregateObjByService.compute_first_availability(meeting.first_availability_updated_at,
                                                             meeting.first_available_date)
        )


async def aggregate_daily_by_calendar(meeting):
    global df_aggregate_daily_by_calendar

    current_date = meeting.created_at
    if meeting.tenant_id not in df_aggregate_daily_by_calendar.values:
        aggregate_obj = AggregateObjByCalendar()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_calendar_id(meeting.calendar_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObjByCalendar.compute_first_availability(meeting.first_availability_updated_at,
                                                              meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(vars(aggregate_obj), ignore_index=True)
    elif meeting.calendar_id not in df_aggregate_daily_by_calendar.values:
        aggregate_obj = AggregateObjByCalendar()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_calendar_id(meeting.calendar_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObj.compute_first_availability(meeting.first_availability_updated_at,
                                                    meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(vars(aggregate_obj), ignore_index=True)
    elif (datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(
            df_aggregate_daily_by_calendar['day'][
                df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id].values[0].split('T')[0],
            '%Y-%m-%d')).days > 0:
        print(df_aggregate_daily_by_calendar)
        print("\n")
        # send current row to clickhouse
        obj_to_insert = \
            df_aggregate_daily_by_calendar[
                df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id].to_dict(
                'records')[0]
        aggregate_obj = AggregateObjByCalendar()
        aggregate_obj.set_day(datetime.strptime(obj_to_insert['day'].split('T')[0], "%Y-%m-%d"))
        aggregate_obj.set_tenant_id(obj_to_insert['tenant_id'])
        aggregate_obj.set_calendar_id(obj_to_insert['calendar_id'])
        aggregate_obj.set_no_of_bookings(obj_to_insert['no_of_bookings'])
        aggregate_obj.set_first_availability(obj_to_insert['first_availability'] / obj_to_insert['no_of_bookings'])
        aggregate_obj.set_week(obj_to_insert['week'])
        aggregate_obj.set_month(obj_to_insert['month'])
        aggregate_obj.set_year(obj_to_insert['year'])
        obj_to_insert = aggregate_obj

        async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            await client.insert([vars(obj_to_insert)], 'calendars_daily_aggregation_by_calendar_v2', 'calendar_id')

        # TODO: export aggregation to /metrics for prometheus
        # await export_metrics(json.loads(df_aggregate_daily_by_calendar[df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id].to_json(orient='records'))[0], 'calendar')

        # delete it from dataframe 
        df_aggregate_daily_by_calendar.drop(
            df_aggregate_daily_by_calendar[df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id].index,
            inplace=True)

        # add row for the new aggregation
        aggregate_obj = AggregateObjByCalendar()
        aggregate_obj.set_day(meeting.created_at)
        aggregate_obj.set_tenant_id(meeting.tenant_id)
        aggregate_obj.set_calendar_id(meeting.calendar_id)
        aggregate_obj.set_no_of_bookings(1)
        aggregate_obj.set_first_availability(
            AggregateObjByCalendar.compute_first_availability(meeting.first_availability_updated_at,
                                                              meeting.first_available_date))
        aggregate_obj.set_week(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%V")))
        aggregate_obj.set_month(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%m")))
        aggregate_obj.set_year(int(datetime.strptime(meeting.created_at.split('T')[0], '%Y-%m-%d').strftime("%G")))

        df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(vars(aggregate_obj), ignore_index=True)
    else:
        df_aggregate_daily_by_calendar.loc[
            df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id, 'no_of_bookings'] += 1
        df_aggregate_daily_by_calendar.loc[
            df_aggregate_daily_by_calendar['calendar_id'] == meeting.calendar_id, 'first_availability'] += (
            AggregateObjByCalendar.compute_first_availability(meeting.first_availability_updated_at,
                                                              meeting.first_available_date)
        )


app.add_route("/metrics", handle_metrics)


async def cleanup_df():
    global df_cleaned_up
    current = datetime.today()
    if time(23, 29, 0) <= current.time() <= time(23, 59, 0) and not df_cleaned_up:
        await insert_old_rows(current, 'calendars_daily_aggregation_by_service_v2', 'service_id')
        await insert_old_rows(current, 'calendars_daily_aggregation_by_calendar_v2', 'calendar_id')
        df_cleaned_up = True
    elif not time(23, 29, 0) <= current.time() <= time(23, 59, 0) and df_cleaned_up:
        df_cleaned_up = False


async def insert_old_rows(time, table_name, aggregation_param):
    df = df_aggregate_daily_by_service if aggregation_param == 'service_id' else df_aggregate_daily_by_calendar
    aggregate_obj = AggregateObjByService() if aggregation_param == 'service_id' else AggregateObjByCalendar()
    rows_to_drop = []
    for idx, row in df.iterrows():
        if datetime.strptime(row['day'].split('T')[0], "%Y-%m-%d") < time:
            aggregate_obj.set_day(datetime.strptime(row['day'].split('T')[0], "%Y-%m-%d"))
            aggregate_obj.set_tenant_id(row['tenant_id'])
            if aggregation_param == 'service_id':
                aggregate_obj.set_service_id(row['service_id'])
            elif aggregation_param == 'calendar_id':
                aggregate_obj.set_calendar_id(row['calendar_id'])
            aggregate_obj.set_no_of_bookings(row['no_of_bookings'])
            aggregate_obj.set_first_availability(row['first_availability'] / row['no_of_bookings'])
            aggregate_obj.set_week(row['week'])
            aggregate_obj.set_month(row['month'])
            aggregate_obj.set_year(row['year'])
            async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
                await client.insert([vars(aggregate_obj)], table_name, aggregation_param)
            rows_to_drop.append(idx)
    if aggregation_param == 'service_id':
        df_aggregate_daily_by_service.drop(rows_to_drop, inplace=True)
    else:
        df_aggregate_daily_by_calendar.drop(rows_to_drop, inplace=True)


async def export_metrics(daily_aggregation, aggregation_param):
    if aggregation_param == 'service':
        REDIRECT_COUNT_SERVICE.labels(
            no_of_bookings=daily_aggregation['no_of_bookings'],
            first_availability=daily_aggregation['first_availability'],
            tenant=daily_aggregation['tenant_id'],
            service=daily_aggregation['service_id']).inc(daily_aggregation['no_of_bookings'])
    elif aggregation_param == 'calendar':
        REDIRECT_COUNT_CALENDAR.labels(
            no_of_bookings=daily_aggregation['no_of_bookings'],
            first_availability=daily_aggregation['first_availability'],
            tenant=daily_aggregation['tenant_id'],
            calendar=daily_aggregation['calendar_id']).inc(daily_aggregation['no_of_bookings'])
    return RedirectResponse(url='0.0.0.0:8000', status_code=302)


if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', proxy_headers=True, forwarded_allow_ips='*', access_log=False)
