# Analitycs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14

## Installation
Run in terminal project `npm i`

## Run dev build

Run `npm run build:elements:dev` for a dev build. Open in browser `run-test-widget.html`.

## Run production build

Run `npm run build:elements:production` for a production build. Build is located in `elements` folder for single build file or `dist` folder for classic build. 

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
