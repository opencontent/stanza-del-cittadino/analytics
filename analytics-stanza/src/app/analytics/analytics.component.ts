import {ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ApiService} from "../api.service";
import {forkJoin, Subject} from "rxjs";
import * as dayjs from 'dayjs'
import dayOfYear from 'dayjs/plugin/dayOfYear'
dayjs.extend(dayOfYear)
import {Services} from "../models/services";
import {delay, takeUntil} from "rxjs/operators";
import {ChartConfiguration, ChartData, ChartEvent, ChartType} from 'chart.js';
import {BaseChartDirective} from "ng2-charts";
import DataLabelsPlugin from 'chartjs-plugin-datalabels';
import {environment} from "../../environments/environment.prod";
import {makeTitle} from "../utility/unslugify";

import {TranslateService} from "@ngx-translate/core";
import {it} from '../i18n/it';
import {de} from '../i18n/de';
import {en} from '../i18n/en';

import {DOCUMENT} from '@angular/common';
import {DropdownSettings} from "../components/multi-dropdown/lib/multiselect.interface";
import { HttpClient } from '@angular/common/http';

import packagejson from '../../../package.json';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnalyticsComponent implements OnInit, OnDestroy {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
  @ViewChild(BaseChartDirective) chart1: BaseChartDirective | undefined;

  private unsubscribe$ = new Subject<void>();
  private tenantId:string = '';
  private href: Location;
  private tenant: string;
  public appVersion: string = packagejson.version;

  loading = true;

  //Box data
  bookings: any
  availability: any = null;

  services: Services[] = []
  itemList = [];
  itemListInterval = [];
  selectedItems = [];
  selectedIntervalItems = [];

  settings : DropdownSettings = {
    singleSelection: false,
    enableSearchFilter: true,
    text: 'Cerca servizio',
    badgeShowLimit: 1,
    groupBy: "service_group",
    disabled: false,
    labelKey: 'name',
    searchBy: ['name'],
    classes: 'custom-multi-select',
    showNullGroup: false
  };

  settingsInterval : DropdownSettings = {
    singleSelection: true,
    enableSearchFilter: false,
    text: 'Seleziona intervallo',
    disabled: false,
    labelKey: 'name',
    classes: 'custom-multi-select',
  };
  /**
   * The constructor.
   *
   * @param ref Detect element changed
   * @param apiService inject api service
   * @param translate the translate service
   * @param document reference page document
   */

  constructor(private ref: ChangeDetectorRef, private apiService: ApiService,private translate: TranslateService,@Inject(DOCUMENT) private document: Document){
    this.href = window.location;
    //Fallback only for development env
    this.tenant = environment.production ? window.location.pathname.split('/')[1] : "comune-di-bugliano"

    const lang = this.document.documentElement.lang
    /* Set language */
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
    /* Load file translation */
    if(lang === 'it'){
      this.translate.setTranslation(lang,it);
    }else if (lang === 'de'){
      this.translate.setTranslation(lang,de);
    }else if(lang === 'en'){
      this.translate.setTranslation(lang,en);
    }else{
      this.translate.setTranslation(lang,it);
    }

  }

  ngOnInit(): void {
    this.loading = true

    forkJoin([
      this.apiService.getServices(this.tenant),
      //this.apiService.getAuth(this.tenant)
    ])
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
             (results) => {
               this.loading = false
               this.services = results[0];

               if(this.services.length){
                 this.tenantId = results[0][0].tenant;
                 // List services
                 this.itemList = results[0].map(x => (x.service_group ? {
                   ...x,
                   service_group: makeTitle(x.service_group)
                 } : x))

                 // Select all services
                 this.selectedItems = this.itemList

                 this.settings = {
                   ...this.settings,
                   text: this.translate.instant('select.find_service'),
                   selectAllText: this.translate.instant('select.select_all_service'),
                   unSelectAllText: this.translate.instant('select.unselect_all_service'),
                   searchPlaceholderText: this.translate.instant('select.find_service'),
                   noDataLabel: this.translate.instant('select.no_service'),
                   filterUnSelectAllText: this.translate.instant('select.filterUnSelectAllText'),
                   filterSelectAllText: this.translate.instant('select.filterSelectAllText')
                 };

                 this.settingsInterval= {
                   ...this.settingsInterval,
                   noDataLabel: this.translate.instant('select.no_interval'),
                 }
                 this.itemListInterval = [
                     {
                   id:"1_days" ,
                   "name":this.translate.instant('select.1_days')},
                   {
                     id:"7_days" ,
                     "name":this.translate.instant('select.7_days')},
                   {
                     id:"28_days" ,
                     "name":this.translate.instant('select.28_days')},
                   {
                     id:"12_months" ,
                     "name":this.translate.instant('select.12_months')},
                   {
                     id:"current_month" ,
                     "name":this.translate.instant('select.current_month')},
                   {
                     id:"previous_month" ,
                     "name":this.translate.instant('select.previous_month')},
                   {
                     id:"current_year" ,
                     "name":this.translate.instant('select.current_year')},
                   {
                     id:"last_year" ,
                     "name":this.translate.instant('select.last_year')}]

                 this.selectedIntervalItems = [
                   {
                     id:"1_days" ,
                     "name":this.translate.instant('select.1_days')}
                 ];

                 this.getStats(this.selectedIntervalItems[0].id)
               }
            //  console.log(results[1])
            },
             (err) => {
               this.loading = false
            }
        );
    this.ref.detectChanges();
  }



  OnSelectInterval(selectValue: any) {
    this.getStats(this.selectedIntervalItems[0].id)
  }


  async getStats(selectValue: string) {

    let start_date: string = dayjs().format('YYYY-MM-DD');
    let end_date: string = dayjs().format('YYYY-MM-DD');
    let interval;

    switch (selectValue) {
      case '1_days':
        interval = "daily"
        end_date = dayjs().subtract(1, 'days').format('YYYY-MM-DD');
        break;
      case '7_days':
        interval = "daily"
        end_date = dayjs().subtract(7, 'days').format('YYYY-MM-DD');
        break;
      case '28_days':
        interval = "daily"
        end_date = dayjs().subtract(28, 'days').format('YYYY-MM-DD');
        break;
      case '12_months':
        interval = "monthly"
        end_date = dayjs().subtract(12, 'month').format('YYYY-MM-DD');
        break;
      case 'current_month':
        interval = "monthly"
        end_date = dayjs().date(1).subtract(0, 'month').format('YYYY-MM-DD');
        break;
      case 'previous_month':
        interval = "monthly"
        end_date = dayjs().subtract(1, 'month').format('YYYY-MM-DD');
        break;
      case 'current_year':
        interval = "yearly"
        end_date = dayjs().dayOfYear(1).subtract(0, 'year').format('YYYY-MM-DD');
        break;
      case 'last_year':
        interval = "yearly"
        end_date = dayjs().subtract(1, 'year').format('YYYY-MM-DD');
        break;
      default:
        return
    }

    this.apiService.getStats(interval,
        end_date,
        start_date,
        this.selectedItems,
        this.tenantId,
        'bookings')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
          this.loading = false
          this.bookings = res
        })

    this.apiService.getStats(interval,
        end_date,
        start_date,
        this.selectedItems,
        this.tenantId,
        'availability')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
          this.loading = false
          this.availability = res;
        })
  }
  /***
   *
   *
   *
   * ***/

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    animation: {
      duration: 2000
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 10
      }
    },
    plugins: {
      legend: {
        display: false,
      },
      datalabels: {
        anchor: 'end',
        align: 'end',
        display: false
      },
      title: {
        display: true,
        text: 'Tasso di saturazione degli appuntamenti'
      }
    }
  };
  public barChartType: ChartType = 'bar';
  public barChartPlugins = [
    DataLabelsPlugin
  ];
  public barChartData: ChartData<'bar'> = {
    labels: [ 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato' ],
    datasets: [
      { data: [ 28, 48, 40, 19, 86, 27 ], label: 'Saturazione' }
    ]
  };

  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
  //  console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
   // console.log(event, active);
  }

  public randomize(): void {
    // Only Change 3 values
    this.barChartData.datasets[0].data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.round(Math.random() * 100),
      56,
      Math.round(Math.random() * 100),
      40 ];

    this.chart?.update();
  }


  // Pie
  public pieChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    animation: {
      duration: 2000
    },
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
        position: 'top',
      },
    title: {
      display: true,
      text: 'Canali di prenotazione'
    },
      datalabels: {
        formatter: (value, ctx) => {
          if (ctx.chart.data.labels) {
            return ctx.chart.data.labels[ctx.dataIndex];
          }
        },
      },
    }
  };
  public pieChartData: ChartData<'pie', number[], string | string[]> = {
    labels: [ 'Desktop', 'Mobile', 'Telefono' ],
    datasets: [ {
      data: [ 10, 10, 80 ]
    } ]
  };
  public pieChartType: ChartType = 'pie';
  public pieChartPlugins = [DataLabelsPlugin ];

  /**
   * Handle select multi-services
   */

  onItemSelect() {
    this.getStats(this.selectedIntervalItems[0].id)
  }



  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
