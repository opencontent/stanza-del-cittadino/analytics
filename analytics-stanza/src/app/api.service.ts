import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Services} from "./models/services";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {}

  getStats(interval? : string, start_date?: string, end_date?: string, select_service?: any[], tenant_id?:string, type?: string){
    let paramsString = '';
    if(interval !== ''){
      paramsString += "interval=" + interval
    }
    if(start_date !== ''){
      paramsString += "&start_date=" + start_date
    }

    if(end_date !== ''){
      paramsString += "&end_date=" + end_date
    }

    paramsString += "&filter=service";

    if(select_service?.length){
      select_service.map(el =>{
        paramsString += "&filter_id=" + el.id
      })
    }
    const params = new HttpParams({
      fromString: paramsString
    });

    if(type === 'bookings'){
      return this.httpClient.get(`${environment.api_analytics}/stats/${tenant_id}/bookings`, {params});
    }else{
      return this.httpClient.get(`${environment.api_analytics}/stats/${tenant_id}/bookings/availability`, {params});
    }

  }

  getServices(tenant: string){
    let api_url;
    if(!environment.production){
      api_url = environment.api_sdc + '/services'
    }else{
      api_url = `/${tenant}/api/services`
    }
    return this.httpClient.get<Services[]>(api_url);
  }

  getAuth(tenant: string){
    let api_url;
    if(!environment.production){
      api_url = environment.api_sdc + '/session-auth'
    }else{
      api_url = `/${tenant}/api/session-auth`
    }
    return this.httpClient.get(api_url)
  }
}
