export const it = {
    "common": {
        "previous_period_var": "Var. periodo precedente",
        "avg_var": "Var. su media"
    },
    "analytics": {
        "title":"Andamento servizi",
        "subtitle": "In quest'area puoi monitorare l'andamento dei servizi digitali del tuo Comune. Per ulteriori analisi e approfondimenti scarica i dati utilizzando la funzione download",
        "reservations": "Prenotazioni",
        "no_of_bookings_previous_period_var": "Var. periodo precedente",
        "no_of_bookings_avg_var": "Var. su media",
        "first_availability": "1° disponibilità"
    },
    "select":{
        "label_service": "Utilizza il filtro per selezionare il servizio/i che vuoi analizzare" ,
        "label_period": "Selezionare il periodo",
        "all_services": "Tutti i servizi",
        "unselect_services": "Deseleziona tutto",
        "find_service": "Cerca servizio",
        "select_all_service": "Seleziona tutti i servizi",
        "unselect_all_service": "Annulla selezione",
        "filterUnSelectAllText": "Deseleziona servizi filtrati",
        "filterSelectAllText": "Seleziona tutti i servizi filtrati",
        "no_service": "Nessun servizio",
        "no_interval": "Non ci sono intervalli",
        "1_days": "Ieri",
        "7_days": "Ultimi 7 gg.",
        "28_days":"Ultimi 28 gg.",
        "12_months":"Ultimi 12 mesi",
        "current_month":"Mese corrente",
        "previous_month":"Mese precedente",
        "current_year":"Anno corrente",
        "last_year":"Anno precedente"
    }
}
