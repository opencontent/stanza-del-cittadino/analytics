export const en = {
    "common": {
        "previous_period_var": "Var. previous period",
        "avg_var": "Var. on average"
    },
    "analytics": {
        "title": "Services trend",
        "subtitle": "In this area you can monitor the progress of your Municipality's digital services. For further analysis and insights, download the data using the download function",
        "reservations": "Reservations",
        "no_of_bookings_previous_period_var": "Var. previous period",
        "no_of_bookings_avg_var": "Var. on media",
        "first_availability": "1st availability"
    },
    "select": {
        "label_service": "Use the filter to select the service (s) you want to analyze",
        "label_period": "Select the period",
        "all_services": "All services",
        "unselect_services": "Deselect all",
        "find_service": "Search service",
        "select_all_service": "Select all services",
        "unselect_all_service": "Deselect all services",
        "filterUnSelectAllText": "Deselect filtered services",
        "filterSelectAllText": "Select all filtered services",
        "no_service": "No service",
        "no_interval": "There are no intervals",
        "1_days": "Yesterday",
        "7_days": "Last 7 days",
        "28_days": "Last 28 days",
        "12_months": "Last 12 months",
        "current_month": "Current month",
        "previous_month": "Previous month",
        "current_year": "Current year",
        "last_year": "Previous year"
    }
}
