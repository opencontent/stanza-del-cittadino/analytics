import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'removeComma'
})
export class RemoveCommaPipe implements PipeTransform {

    transform(value: string): string {
        let decimals = 2;
        let d = Math.pow(10,decimals);
        // @ts-ignore
        return (parseInt(String(value * d))/d).toFixed(decimals);
    }
}
