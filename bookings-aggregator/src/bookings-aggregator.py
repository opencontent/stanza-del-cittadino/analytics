from random import randint
from typing import Set, Any
from fastapi import FastAPI, status
from numpy import unravel_index
from pydantic import BaseModel
from itsdangerous import exc
from kafka import TopicPartition
from datetime import timedelta, datetime, time
from prometheus_client import Counter
from starlette.responses import RedirectResponse
from starlette_exporter import PrometheusMiddleware, handle_metrics
from pythonjsonlogger import jsonlogger
from dotenv import load_dotenv
from signal import signal, SIGINT
from sys import exit
from controllers.ClickHouseClient import ClickHouseClient

import uvicorn
import aiokafka
import asyncio
import json
import logging
import os
import traceback
import re
import pandas as pd


# instantiate the API
app = FastAPI()

load_dotenv()

# global variables
consumer_task = None
consumer = None
#_state = 0

# env variables
KAFKA_TOPIC = os.getenv('KAFKA_TOPIC', default='applications')
KAFKA_CONSUMER_GROUP_PREFIX = os.getenv('KAFKA_CONSUMER_GROUP_PREFIX', default='analytics-bookings-aggregator')
KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS', default='localhost:29092')
PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='analytics_bookings_aggregator')
CLICKHOUSE_HOST = os.getenv('CLICKHOUSE_HOST', default='localhost')
CLICKHOUSE_USER = os.getenv('CLICKHOUSE_USER', default='')
CLICKHOUSE_SECRET = os.getenv('CLICKHOUSE_SECRET', default='')
CLICKHOUSE_DB = os.getenv('CLICKHOUSE_DB', default='calendar_analytics')  

REDIRECT_COUNT_SERVICE = Counter("bookings_by_service_total", "Count of bookings by service", ("no_of_bookings", "tenant", "service"))
REDIRECT_COUNT_CALENDAR = Counter("bookings_by_calendar_total", "Count of bookings by calendar", ("no_of_bookings", "tenant", "calendar"))
df_aggregate_daily_by_service = pd.DataFrame(columns=['day', 'tenant_id', 'service_id', 'no_of_bookings', 'week', 'month', 'year'])
df_aggregate_daily_by_calendar = pd.DataFrame(columns=['day', 'tenant_id', 'calendar_id', 'no_of_bookings', 'week', 'month', 'year'])
# check if the aggregations has been cleaned once in a day
df_cleaned_up = False

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)

log = logging.getLogger(__name__)
class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)  

class Status(BaseModel):
    status: str

class HealthCheck(BaseModel):
    healthcheck: str

@app.on_event("startup")
async def startup_event():
    log.info('Initializing API ...')
    logger = logging.getLogger('uvicorn.access')
    logHandler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    logHandler.setFormatter(formatter)
    logger.addHandler(logHandler)

    await initialize()
    await consume()

@app.on_event("shutdown")
async def shutdown_event():
    log.info('Shutting down API')
    consumer_task.cancel()
    await consumer.stop()

@app.get('/status', response_model=Status, status_code=status.HTTP_200_OK)
async def perform_healthcheck():
    await cleanup_df()
    return {'status': 'Everything OK!'}

@app.get('/healthcheck', response_model=HealthCheck, status_code=status.HTTP_200_OK)
async def perform_healthcheck():
    return {'healthcheck': 'Everything OK!'}

async def initialize():
    # loop = asyncio.get_event_loop()
    global consumer
    group_id = f'{KAFKA_CONSUMER_GROUP_PREFIX}'
    log.debug(f'Initializing KafkaConsumer for topic {KAFKA_TOPIC}, group_id {group_id}'
              f' and using bootstrap servers {KAFKA_BOOTSTRAP_SERVERS}')
    consumer = aiokafka.AIOKafkaConsumer(KAFKA_TOPIC,
                                         bootstrap_servers=[broker for broker in KAFKA_BOOTSTRAP_SERVERS.split(',')],
                                         group_id=group_id,
                                         enable_auto_commit=False,
                                         auto_offset_reset='earliest')
    # get cluster layout and join group
    await consumer.start()

    partitions: Set[TopicPartition] = consumer.assignment()
    nr_partitions = len(partitions)
    if nr_partitions != 1:
        log.warning(f'Found {nr_partitions} partitions for topic {KAFKA_TOPIC}. Expecting '
                    f'only one, remaining partitions will be ignored!')
    for tp in partitions:
        # get the log_end_offset
        end_offset_dict = await consumer.end_offsets([tp])
        end_offset = end_offset_dict[tp]

        if end_offset == 0:
            log.warning(f'Topic ({KAFKA_TOPIC}) has no messages (log_end_offset: '
                        f'{end_offset}), skipping initialization ...')
            return

        log.debug(f'Found log_end_offset: {end_offset} seeking to {end_offset-1}')
        # consumer.seek(tp, end_offset-1)
        await consumer.seek_to_beginning(tp)
        msg = await consumer.getone()
        log.info(f'Initializing API with data from msg: {msg}')

        # update the API state
        #_update_state(msg)
        return

async def consume():
    global consumer_task
    consumer_task = asyncio.create_task(send_consumer_message(consumer))

async def send_consumer_message(consumer):
    try:
        most_recent_aggregation_by_service = await get_most_recent_date('calendars_daily_aggregation_by_service', 'service_id')
        most_recent_aggregation_by_calendar = await get_most_recent_date('calendars_daily_aggregation_by_calendar', 'calendar_id')
        # consume messages
        async for msg in consumer:
            application = json.loads(msg.value)
            # print(rating)
            if 'submitted_at' in application and application['data'] is not None:
                if application['submitted_at'] is not None and 'calendar' in application['data']:
                    if len(most_recent_aggregation_by_service) != 0:
                        if len([item for item in most_recent_aggregation_by_service if item[1] == application['tenant'] and item[2] == application['service_id']]) != 0:
                            submitted_at = datetime.strptime(application['submitted_at'].split('T')[0], "%Y-%m-%d")
                            most_recent_aggregation = [item for item in most_recent_aggregation_by_service if datetime.combine(item[0], datetime.min.time()) < submitted_at and item[1] == application['tenant'] and item[2] == application['service_id']]
                            if len(most_recent_aggregation) != 0:
                                await aggregate_daily_by_service(application)
                        else:
                            await aggregate_daily_by_service(application)
                    else:
                        await aggregate_daily_by_service(application)
                    if len(most_recent_aggregation_by_calendar) != 0:
                        calendar_info = [x for x in re.split('[@(#)\s]', application['data']['calendar']) if x.strip()]
                        if len([item for item in most_recent_aggregation_by_calendar if item[1] == application['tenant'] and item[2] == calendar_info[2]]) != 0:
                            submitted_at = datetime.strptime(application['submitted_at'].split('T')[0], "%Y-%m-%d")
                            most_recent_aggregation = [item for item in most_recent_aggregation_by_calendar if datetime.combine(item[0], datetime.min.time()) < submitted_at and item[1] == application['tenant'] and item[2] == calendar_info[2]]
                            if len(most_recent_aggregation) != 0:   
                                await aggregate_daily_by_calendar(application)
                        else:
                            await aggregate_daily_by_calendar(application)
                    else:
                        await aggregate_daily_by_calendar(application)
    except Exception as e:
        traceback.print_exc()
    finally:
        # will leave consumer group; perform autocommit if enabled
        log.warning('Stopping consumer')
        await consumer.stop()
        exit(1)

async def get_most_recent_date(table_name, aggregation_param):
    async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
        most_recent_date = await client.get_most_recent_date(table_name, aggregation_param)
    return most_recent_date

async def aggregate_daily_by_service(application):
    global df_aggregate_daily_by_service
    try:
        current_date = application['submitted_at']
        if(application['tenant'] not in df_aggregate_daily_by_service.values):
            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'service_id': application['service_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            } 
            df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(aggregate_obj, ignore_index=True)   
        elif(application['service_id'] not in df_aggregate_daily_by_service.values):
            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'service_id': application['service_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            } 
            df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(aggregate_obj, ignore_index=True) 
        elif((datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily_by_service['day'][df_aggregate_daily_by_service['service_id'] == application['service_id']].values[0].split('T')[0], '%Y-%m-%d')).days > 0):
            # days_difference = (datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily_by_service['day'][df_aggregate_daily_by_service['service_id'] == application['service_id']].values[0].split('T')[0], '%Y-%m-%d')).days 
            # if(days_difference > 1):
            #     print(df_aggregate_daily_by_service)
            #     print("\n")    
            #     # send current row to clickhouse
            #     # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')
            #     with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            #         # await client.insert_by_service(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_json(orient='records'))
            #         await client.insert_by_service(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_dict('records'))
                    
            #     # TODO: export aggregation to /metrics for prometheus
            
            #     # delete it from dataframe 
            #     df_aggregate_daily_by_service.drop(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].index, inplace=True)

            #     for i in range(days_difference-1, 0, -1):
            #         timestamp = datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d') - timedelta(days=i)
            #         week = int(timestamp.strftime("%V")) 
            #         month = int(timestamp.strftime("%m"))
            #         year = int(timestamp.strftime("%G"))

            #         aggregate_obj = {
            #             'day': timestamp, 
            #             'tenant_id': application['tenant'],
            #             'service_id': application['service_id'],
            #             'no_of_bookings': 0,
            #             'week': week,
            #             'month': month,
            #             'year': year
            #         }
            #         df_to_insert = pd.DataFrame.from_dict([aggregate_obj])
            #         # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(json.dumps([aggregate_obj]), 'daily')
            #         with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            #             await client.insert_by_service(df_to_insert.to_dict('records'))
                
            #         # for j in range(1,6):
            #         #     await export_metrics(aggregate_obj, j)
                
            #     week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            #     month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            #     year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            #     # create and add a new one
            #     aggregate_obj = {
            #         'day': application['submitted_at'], 
            #         'tenant_id': application['tenant'],
            #         'service_id': application['service_id'],
            #         'no_of_bookings': 1,
            #         'week': week,
            #         'month': month,
            #         'year': year
            #     }
            #     df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(aggregate_obj, ignore_index=True)     
            # else:
            print(df_aggregate_daily_by_service)
            print("\n")    
            # TODO: send current row to clickhouse
            # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')
            async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
                # await client.insert_by_service(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_json(orient='records'))
                await client.insert(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_dict('records'), 'calendars_daily_aggregation_by_service', 'service_id')

            # TODO: export aggregation to /metrics for prometheus
            # await export_metrics(json.loads(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_json(orient='records'))[0], 'service')

            # delete it from dataframe 
            df_aggregate_daily_by_service.drop(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].index, inplace=True)

            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            # create and add a new one
            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'service_id': application['service_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            }
            df_aggregate_daily_by_service = df_aggregate_daily_by_service.append(aggregate_obj, ignore_index=True)
        else:
            df_aggregate_daily_by_service['no_of_bookings'][df_aggregate_daily_by_service['service_id'] == application['service_id']] += 1
    except TypeError:
        pass

async def aggregate_daily_by_calendar(application):
    global df_aggregate_daily_by_calendar
    try:
        current_date = application['submitted_at']
        calendar_info = [x for x in re.split('[@(#)\s]', application['data']['calendar']) if x.strip()]
        # if(len(calendar_info) > 2):
        #     calendar_info = {'booking_date':calendar_info[0], 'booking_time':calendar_info[1], 'calendar_id':calendar_info[2], 'meeting_id':calendar_info[3], 'opening_hours_id':calendar_info[4]}
        # else:
        calendar_info = {'booking_date':calendar_info[0], 'booking_time':calendar_info[1], 'calendar_id':calendar_info[2]}
        if(application['tenant'] not in df_aggregate_daily_by_calendar.values):
            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'calendar_id': calendar_info['calendar_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            } 
            df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(aggregate_obj, ignore_index=True)   
        elif(calendar_info['calendar_id'] not in df_aggregate_daily_by_calendar.values):
            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'calendar_id': calendar_info['calendar_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            } 
            df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(aggregate_obj, ignore_index=True) 
        elif((datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily_by_calendar['day'][df_aggregate_daily_by_calendar['calendar_id'] == calendar_info['calendar_id']].values[0].split('T')[0], '%Y-%m-%d')).days > 0):
            # days_difference = (datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily_by_calendar['day'][df_aggregate_daily_by_calendar['calendar_id'] == calendar_info['calendar_id']].values[0].split('T')[0], '%Y-%m-%d')).days 
            # if(days_difference > 1):
            #     print(df_aggregate_daily_by_calendar)
            #     print("\n")    
            #     # send current row to clickhouse
            #     # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')
                
            #     # TODO: export aggregation to /metrics for prometheus
            
            #     # delete it from dataframe 
            #     df_aggregate_daily_by_calendar.drop(df_aggregate_daily_by_calendar[df_aggregate_daily_by_calendar['calendar_id'] == calendar_info[2]].index, inplace=True)

            #     for i in range(days_difference-1, 0, -1):
            #         timestamp = datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d') - timedelta(days=i)
            #         week = int(timestamp.strftime("%V")) 
            #         month = int(timestamp.strftime("%m"))
            #         year = int(timestamp.strftime("%G"))

            #         aggregate_obj = {
            #             'day': application['submitted_at'], 
            #             'tenant_id': application['tenant'],
            #             'calendar_id': calendar_info['calendar_id'],
            #             'no_of_bookings': 0,
            #             'week': week,
            #             'month': month,
            #             'year': year
            #         }
            #         # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(json.dumps([aggregate_obj]), 'daily')
            #         # for j in range(1,6):
            #         #     await export_metrics(aggregate_obj, j)
                
            #     week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            #     month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            #     year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            #     # create and add a new one
            #     aggregate_obj = {
            #         'day': application['submitted_at'], 
            #         'tenant_id': application['tenant'],
            #         'calendar_id': calendar_info['calendar_id'],
            #         'no_of_bookings': 1,
            #         'week': week,
            #         'month': month,
            #         'year': year
            #     }
            #     df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(aggregate_obj, ignore_index=True)     
            # else:
            print(df_aggregate_daily_by_calendar)
            print("\n")    
            # TODO: send current row to clickhouse
            # await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')
            async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
                # await client.insert_by_service(df_aggregate_daily_by_service[df_aggregate_daily_by_service['service_id'] == application['service_id']].to_json(orient='records'))
                await client.insert(df_aggregate_daily_by_calendar[df_aggregate_daily_by_calendar['calendar_id'] == calendar_info['calendar_id']].to_dict('records'), 'calendars_daily_aggregation_by_calendar', 'calendar_id')

            # TODO: export aggregation to /metrics for prometheus
            # await export_metrics(json.loads(df_aggregate_daily_by_service[df_aggregate_daily_by_service['calendar_id'] == application['calendar_id']].to_json(orient='records'))[0], 'calendar')

            # delete it from dataframe 
            df_aggregate_daily_by_calendar.drop(df_aggregate_daily_by_calendar[df_aggregate_daily_by_calendar['calendar_id'] == calendar_info['calendar_id']].index, inplace=True)

            week = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(application['submitted_at'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            # create and add a new one
            aggregate_obj = {
                'day': application['submitted_at'], 
                'tenant_id': application['tenant'],
                'calendar_id': calendar_info['calendar_id'],
                'no_of_bookings': 1,
                'week': week,
                'month': month,
                'year': year
            }
            df_aggregate_daily_by_calendar = df_aggregate_daily_by_calendar.append(aggregate_obj, ignore_index=True)
        else:
            df_aggregate_daily_by_calendar['no_of_bookings'][df_aggregate_daily_by_calendar['calendar_id'] == calendar_info['calendar_id']] += 1
    except TypeError:
        pass

app.add_route("/metrics", handle_metrics)

async def cleanup_df():
    global df_cleaned_up
    current = datetime.today()
    if time(23, 29, 0) <= current.time() <= time(23, 59, 0) and not df_cleaned_up:
        await insert_old_rows(current, 'calendars_daily_aggregation_by_service', 'service_id')
        await insert_old_rows(current, 'calendars_daily_aggregation_by_calendar', 'calendar_id')
        df_cleaned_up = True
    elif not time(23, 29, 0) <= current.time() <= time(23, 59, 0) and df_cleaned_up:
        df_cleaned_up = False

async def insert_old_rows(time, table_name, aggregation_param):
    df = df_aggregate_daily_by_service if aggregation_param == 'service_id' else df_aggregate_daily_by_calendar 
    rows_to_drop = []
    for idx, row in df.iterrows():
        if datetime.strptime(row['day'].split('T')[0], "%Y-%m-%d") < time:
            async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
                await client.insert([row.to_dict()], table_name, aggregation_param)
            rows_to_drop.append(idx)
    if aggregation_param == 'service_id':
        df_aggregate_daily_by_service.drop(rows_to_drop, inplace=True)
    else:
        df_aggregate_daily_by_calendar.drop(rows_to_drop, inplace=True) 

async def export_metrics(daily_aggregation, aggregation_param):
#     if not any(el.get('entrypoint-id') == daily_aggregation['entrypoint_id'] for el in entrypoints_and_tenants_list):
#         entrypoint_and_tenant_name = await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query(daily_aggregation)
#         if('data' in entrypoint_and_tenant_name and len(entrypoint_and_tenant_name['data']['entrypoints']) != 0):
#             REDIRECT_COUNT.labels(
#                         star_rating = star_index, 
#                         tenant = entrypoint_and_tenant_name['data']['entrypoints'][0]['entrypoints_to_tenants']['name'], 
#                         entrypoint = entrypoint_and_tenant_name['data']['entrypoints'][0]['name']).inc(daily_aggregation['values_' + str(star_index) + '_count'])
#             entrypoints_and_tenants_list.append({
#                 'entrypoint-id': daily_aggregation['entrypoint_id'],
#                 'entrypoint-name': entrypoint_and_tenant_name['data']['entrypoints'][0]['name'],
#                 'tenant-name': entrypoint_and_tenant_name['data']['entrypoints'][0]['entrypoints_to_tenants']['name'] 
#             })
#     else:
#         REDIRECT_COUNT.labels(
#                     star_rating = star_index,
#                     tenant = [el['tenant-name'] for el in entrypoints_and_tenants_list if el['entrypoint-id'] == daily_aggregation['entrypoint_id']][0], 
#                     entrypoint = [el['entrypoint-name'] for el in entrypoints_and_tenants_list if el['entrypoint-id'] == daily_aggregation['entrypoint_id']][0]).inc(daily_aggregation['values_' + str(star_index) + '_count'])  
    if aggregation_param == 'calendar':
        REDIRECT_COUNT_SERVICE.labels(
                            no_of_bookings = daily_aggregation['no_of_bookings'],
                            tenant = daily_aggregation['tenant_id'],
                            calendar = daily_aggregation['calendar_id'])
    elif aggregation_param == 'service':
        REDIRECT_COUNT_CALENDAR.labels(
                            no_of_bookings = daily_aggregation['no_of_bookings'],
                            tenant = daily_aggregation['tenant_id'],
                            service = daily_aggregation['service_id'])
    return RedirectResponse(url='0.0.0.0:8000', status_code=302)

if __name__ == "__main__":
    # uvicorn.run(app, host=FASTAPI_APP_HOST, port=int(FASTAPI_APP_PORT), proxy_headers=True, forwarded_allow_ips='*', access_log=False)
    uvicorn.run(app, host='0.0.0.0', proxy_headers=True, forwarded_allow_ips='*', access_log=False)