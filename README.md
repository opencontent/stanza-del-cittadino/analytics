# Bookings-aggregator

Questo componente è parte degli Analytics dei calendari della Stanza del Cittadino

## Cosa fa

Questo componente, mediante FastAPI, legge da kafka le pratiche (in futuro anche gli appuntamenti) della stanza del cittadino e le aggrega giornalmente per tenant e servizio/calendario, e calcola il numero di prenotazioni, la prima disponibilità media, la capacità oraria, la saturazione degli appuntamenti e la percentuale degli appuntamenti cancellati e rinviati

## Monitoring

Il microservizio espone anche in formato prometheus metriche e statistiche generiche sulle comunicazioni tra il microservizio e kafka e il microservizio e il db per individuare eventuali errori.

## Configurazione

Il microservizio è configurabile mediante variabili di ambiente

| Nome                        | Default                           | Required | Description                                                |
| --------------------------- | --------------------------------- | -------- | ---------------------------------------------------------- |
| PROMETHEUS_JOB_NAME         | analytics_bookings_aggregator     | Yes      | Identificativo delle metriche esposte dal microservizio    |
| CH_HOST                     | clickhouse-server                 | Yes      | indirizzo del database dal quale prendere le aggregazioni  |
| CH_USER                     | default                           | No       | username dell'utente che richiede la connessione al db     |
| CH_SECRET                   |                                   | No       | password del database del quale si richiede la connessione |
| CH_DB                       | sdc_analytics                     | Yes      | Nome del database al quale ci si vuole connettere          |
| KAFKA_TOPIC                 | applications                      | Yes      | Topic di kafka dal quale leggere le pratiche               |
| KAFKA_CONSUMER_GROUP_PREFIX | sdc_analytics_bookings_aggregator | Yes      | Nome del consumer group tramite il quale leggere da kafka  |
| KAFKA_BOOTSTRAP_SERVERS     |                                   | Yes      | Endpoint esposto dal servizio di kafka                     |


# Charts-exporter

Questo componente è parte degli Analytics dei calendari della Stanza del Cittadino

## Cosa fa

Questo componente, mediante FastAPI, interroga il database di ClickHouse per ottenere i KPI giornalieri, settimanali, mensili e annuali. Successivamente, i dati vengono formattati per ChartJS e infine restituiti per essere rappresentati sui grafici creati con quest'ultimo.

## Documentazione

Le API disponibili vengono esposte sul path `[/docs](https://sdc-analytics-dev-charts-exporter.boat.opencontent.io/docs)`

## Monitoring

Il microservizio espone anche in formato prometheus metriche e statistiche generiche sulla durata delle richieste.

## Deploy

Il microservizio è disponibile al seguente [link](https://sdc-analytics-dev-charts-exporter.boat.opencontent.io/docs), dove si possono provare le diverse API offerte

Sul path `/status` è disponibile una url di controllo per verificare che l'applicazione sta funzionando correttamente.

## Configurazione

Il microservizio è configurabile mediante variabili di ambiente

| Nome                | Default                   | Required | Description                                                                    |
| ------------------- | ------------------------- | -------- | ------------------------------------------------------------------------------ |
| PROMETHEUS_JOB_NAME | analytics_charts_exporter | Yes      | Identificativo delle metriche esposte dal microservizio                        |
| MAX_DAYS            | 180                       | Yes      | Numero massimo di aggregazioni giornaliere che il microservizio può restituire |
| MAX_WEEKS           | 52                        | Yes      | Numero massimo di aggregazioni settimanali che il servizio può restituire      |
| MAX_MONTHS          | 18                        | Yes      | Numero massimo di aggregazioni mensili che il servizio può restituire          |
| MAX_YEARS           | 3                         | Yes      | Numero massimo di aggregazioni annuali che il servizio può restituire          |
| CH_HOST             | clickhouse-server         | Yes      | indirizzo del database dal quale prendere le aggregazioni                      |
| CH_USER             | default                   | No       | username dell'utente che richiede la connessione al db                         |
| CH_SECRET           |                           | No       | password del database del quale si richiede la connessione                     |
| CH_DB               | sdc_analytics             | Yes      | Nome del database al quale ci si vuole connettere                              |