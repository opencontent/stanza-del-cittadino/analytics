const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
  const files = [
    './dist/analytics/main.js',
    './dist/analytics/polyfills.js'
  ]
  await fs.ensureDir('elements')
  await concat(files, 'elements/analytics.js')})()
