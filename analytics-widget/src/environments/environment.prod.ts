export const environment = {
  production: true,
  api_analytics: 'https://sdc-analytics-dev-charts-exporter.boat.opencontent.io',
  api_sdc: '/api'
};
