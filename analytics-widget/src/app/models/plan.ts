export enum PlanLevel {
    FREE,
    PRO,
    FULL
}

/**
 * This is equivalent to:
 * type PlanLevelStrings = 'FREE' | 'PRO' | 'FULL';
 */
type PlanLevelStrings = keyof typeof PlanLevel;

export function enableByPlan(p: PlanLevelStrings, data: any[]) {
   // const num = PlanLevel[key];
    return data.filter( el =>  el.plan <= p )
}

export function levelOfPlan(key: PlanLevelStrings) {
    return PlanLevel[key];
}
