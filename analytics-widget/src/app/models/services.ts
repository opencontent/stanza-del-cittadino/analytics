export interface Parameters {
    formio_id: string;
    url: string;
}

export interface FlowStep {
    identifier: string;
    title?: any;
    type: string;
    description?: any;
    guide?: any;
    parameters: Parameters;
}

export interface ProtocolloParameters {
}

export interface Services {
    id: string;
    name: string;
    slug: string;
    tenant: string;
    topics: string;
    topics_id: string;
    description: string;
    howto: string;
    who: string;
    special_cases: string;
    more_info: string;
    compilation_info: string;
    final_indications: string;
    coverage: string[];
    response_type?: any;
    flow_steps: FlowStep[];
    protocol_required?: boolean;
    protocol_handler: string;
    protocollo_parameters: ProtocolloParameters;
    payment_required?: number;
    payment_parameters: any[];
    integrations: any;
    sticky: boolean;
    status: number;
    access_level: number;
    login_suggested: boolean;
    scheduled_from?: any;
    scheduled_to?: any;
    service_group: string;
    service_group_id: string;
    shared_with_group: boolean;
    allow_reopening: boolean;
    allow_withdraw: boolean;
    allow_integration_request: boolean;
    workflow: number;
    recipients: string[];
    recipients_id: string[];
    geographic_areas: string[];
    geographic_areas_id: string[];
}

