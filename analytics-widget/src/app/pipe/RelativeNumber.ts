import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe check relative number
 */
@Pipe({
    name: 'relativeNumber'
})
export class RelativeNumber implements PipeTransform {
    transform(item: number): any {
        if(item > 0){
            return '+' + item + ' %';
        }else if(item == null){
           return 'n.d'
        }else{
            return item.toString() + ' %'
        }

    }
}

