export const de = {
    "common": {
        "previous_period_var": "Änd. in der Vorperiode",
        "avg_var": "Var. im Durchschnitt"
    },
    "analytics": {
        "title": "Erbringung von Dienstleistungen",
        "subtitle": "In diesem Bereich können Sie den Fortschritt der digitalen Dienste Ihrer Gemeinde überwachen. Für weitere Analysen und Einblicke laden Sie die Daten über die Download-Funktion herunter",
        "reservations": "Reservierungen",
        "no_of_bookings_previous_period_var": "Var. Vorperiode",
        "no_of_bookings_avg_var": "Var. Ihr Durchschnitt",
        "first_availability": "1. Verfügbarkeit",
        "applications_count" : "Praktiken Methoden Ausübungen",
        "active_users" : "Aktive Benutzer",
        "applications_by_user" : "Praktiken pro Benutzer"
    },
    "select": {
        "label_service": "Verwenden Sie den Filter, um die Dienste auszuwählen, die Sie analysieren möchten.",
        "label_period": "Wählen Sie den Zeitraum",
        "all_services": "Alle Dienstleistungen",
        "unselect_services": "Auswahl abbrechen",
        "find_service": "Suchdienst",
        "select_all_service": "Wählen Sie alle Dienstleistungen",
        "unselect_all_service": "Alle Dienste abwählen",
        "filterUnSelectAllText": "Deaktivieren Sie gefilterte Dienste",
        "filterSelectAllText": "Wählen Sie alle gefilterten Dienste aus",
        "no_service": "Kein Service",
        "no_interval": "Es gibt keine Lücken",
        "1_days": "Gestern",
        "7_days": "Letzte 7 Tage.",
        "28_days": "Letzte 28 Tage.",
        "12_months": "Letzte 12 Monate",
        "current_month": "Aktueller Monat",
        "previous_month": "Vorheriger Monat",
        "current_year": "Laufendes Jahr",
        "last_year": "Letztes Jahr"
    },
    "charts":{
        'booking_channels':'Buchungskanäle',
        'appointment_saturation_rate': 'Terminsättigungsrate',
        'applications_for_week': 'Übungen nach Wochentag',
        'applications_for_channel': 'Praktiken nach Kanal',
        'applications_for_hours': 'Übungen nach Zeitfenster',
        'applications_outcome': 'Ergebnis',

    },
    "tabs":{
        "bookings": "Termine",
        "use_services": "Nutzung von Diensten"
    },
    "errors":{
        "alert_tenant" : "<h3>Vorsicht</h3> es liegen falsche oder fehlende Konfigurationen vor!"
    }
}
