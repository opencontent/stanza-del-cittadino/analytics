import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {APP_BASE_HREF, CommonModule} from "@angular/common";
import {createCustomElement} from "@angular/elements";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AnalyticsComponent} from "./analytics/analytics.component";
import {NgChartsModule} from "ng2-charts";
import {TranslateModule} from "@ngx-translate/core";
import {ErrorInterceptor} from "./interception/error.interceptor";
import {RequestInterceptor} from "./interception/request.interceptor";
import {AngularMultiSelectCustomModule} from "./components/multi-dropdown/lib/multiselect.component";
import {RelativeNumber} from "./pipe/RelativeNumber";
import {LoaderComponent} from "./components/loader/loader.component";
import {CountUpDirective} from './directives/count-up.directive';
import { FormsModule } from '@angular/forms';
import {RemoveCommaPipe} from "./pipe/RemoveCommaPipe";
import '@angular/common/locales/global/it';
import {BarComponent} from "./components/charts/bar/bar.component";
import { PieComponent } from './components/charts/pie/pie.component';
import { PeriodLabelsComponent } from './components/card/period-labels/period-labels.component';


@NgModule({
    declarations: [
        AnalyticsComponent,
        RelativeNumber,
        LoaderComponent,
        CountUpDirective,
        RemoveCommaPipe,
        BarComponent,
        PieComponent,
        PeriodLabelsComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        NgChartsModule,
        AngularMultiSelectCustomModule,
        TranslateModule.forRoot({
            defaultLanguage: 'it'
        }),
    ],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true,
            deps: [],
        }
    ],
})
export class AppModule {
    constructor(private injector: Injector) {
    }

    ngDoBootstrap() {
        const el = createCustomElement(AnalyticsComponent, {
            injector: this.injector
        });
        customElements.define('app-analytics', el);
    }
}
