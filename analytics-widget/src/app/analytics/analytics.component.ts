import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {ApiService} from "../api.service";
import {forkJoin, Subject} from "rxjs";
import * as dayjs from 'dayjs'
import dayOfYear from 'dayjs/plugin/dayOfYear'
import {Services} from "../models/services";
import {takeUntil} from "rxjs/operators";
import {environment} from "../../environments/environment.prod";
import {makeTitle} from "../utility/unslugify";

import {TranslateService} from "@ngx-translate/core";
import {it} from '../i18n/it';
import {de} from '../i18n/de';
import {en} from '../i18n/en';

import {DOCUMENT} from '@angular/common';
import {DropdownSettings} from "../components/multi-dropdown/lib/multiselect.interface";

import packagejson from '../../../package.json';
import {enableByPlan, levelOfPlan, PlanLevel} from "../models/plan";

dayjs.extend(dayOfYear)

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnalyticsComponent implements OnInit, OnDestroy {

  @ContentChild('tab') tab: ElementRef;

  private unsubscribe$ = new Subject<void>();
  private tenantId: string = '';
  private href: Location;
  private tenant: string;
  public appVersion: string = packagejson.version;

  loading = true;

  //Box data
  bookings: any
  availability: any;
  applications_count: any;
  active_users: any;
  applications_by_user: any;
  chartsBar_applications_and_users_by_time_slot: any;

  services: Services[] = []
  itemList = [];
  itemListInterval = [];
  selectedItems = [];
  selectedIntervalItems = [];

  settings: DropdownSettings = {
    singleSelection: false,
    enableSearchFilter: true,
    text: 'Cerca servizio',
    badgeShowLimit: 1,
    groupBy: "service_group",
    disabled: false,
    labelKey: 'name',
    searchBy: ['name'],
    classes: 'custom-multi-select',
    showNullGroup: false
  };

  settingsInterval: DropdownSettings = {
    singleSelection: true,
    enableSearchFilter: false,
    text: 'Seleziona intervallo',
    disabled: false,
    labelKey: 'name',
    classes: 'custom-multi-select',
  };
  tab1 = true;
  tab2 = false;
  charts: any = undefined;
  chartsPie: any = undefined;
  chartsPie1: any = undefined;
  chartsBarUser: any = undefined;
  chartsBarUser1: any = undefined;
  plan: PlanLevel ;
  validate = true;


  /**
   * The constructor.
   *
   * @param ref Detect element changed
   * @param apiService inject api service
   * @param translate the translate service
   * @param document reference page document
   * @param elementRef tag-app data-options
   */

  constructor(
      private ref: ChangeDetectorRef,
      private apiService: ApiService,
      private translate: TranslateService,
      @Inject(DOCUMENT) private document: Document,
      private elementRef: ElementRef
  ) {

    this.href = window.location;
    //Fallback only for development env
    this.tenant = environment.production ? window.location.pathname.split('/')[1] : "comune-di-bugliano"

    const lang = this.document.documentElement.lang
    /* Set language */
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
    /* Load file translation */
    if (lang === 'it') {
      this.translate.setTranslation(lang, it);
    } else if (lang === 'de') {
      this.translate.setTranslation(lang, de);
    } else if (lang === 'en') {
      this.translate.setTranslation(lang, en);
    } else {
      this.translate.setTranslation(lang, it);
    }

    // Get config tenant plan - fallback value "FREE"
    this.plan = levelOfPlan(this.validateConfTenant(this.elementRef.nativeElement.getAttribute('data-config')))
  }

  ngOnInit(): void {
    this.loading = true

    forkJoin([
      this.apiService.getServices(this.tenant),
      //this.apiService.getAuth(this.tenant)
    ]).pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (results) => {
              this.loading = false;
              this.services = results[0];
              if (this.services.length) {
                this.tenantId = results[0][0].tenant;

                /**
                 * Unslugify service_group attribute
                 */
                this.itemList = results[0].map(x => (x.service_group ? {
                  ...x,
                  service_group: makeTitle(x.service_group)
                } : x));

                /**
                 * Set default value select filter service - all service selected
                 */
                this.selectedItems = this.itemList

                /**
                 Translate options select filter
                 **/
                this.settings = {
                  ...this.settings,
                  text: this.translate.instant('select.find_service'),
                  selectAllText: this.translate.instant('select.select_all_service'),
                  unSelectAllText: this.translate.instant('select.unselect_all_service'),
                  searchPlaceholderText: this.translate.instant('select.find_service'),
                  noDataLabel: this.translate.instant('select.no_service'),
                  filterUnSelectAllText: this.translate.instant('select.filterUnSelectAllText'),
                  filterSelectAllText: this.translate.instant('select.filterSelectAllText')
                };

                this.settingsInterval = {
                  ...this.settingsInterval,
                  noDataLabel: this.translate.instant('select.no_interval'),
                }

                this.itemListInterval = [
                  {
                    id: "1_days",
                    "name": this.translate.instant('select.1_days'),
                    "plan": PlanLevel.FREE
                  },
                  {
                    id: "7_days",
                    "name": this.translate.instant('select.7_days'),
                    "plan": PlanLevel.FREE
                  },
                  {
                    id: "28_days",
                    "name": this.translate.instant('select.28_days'),
                    "plan": PlanLevel.FREE
                  },
                  {
                    id: "12_months",
                    "name": this.translate.instant('select.12_months'),
                    "plan": PlanLevel.FREE
                  },
                  {
                    id: "current_month",
                    "name": this.translate.instant('select.current_month'),
                    "plan": PlanLevel.FULL
                  },
                  {
                    id: "previous_month",
                    "name": this.translate.instant('select.previous_month'),
                    "plan": PlanLevel.FULL
                  },
                  {
                    id: "current_year",
                    "name": this.translate.instant('select.current_year'),
                    "plan": PlanLevel.FULL
                  },
                  {
                    id: "last_year",
                    "name": this.translate.instant('select.last_year'),
                    "plan": PlanLevel.FULL
                  }
                  ]

                /**
                 * Set default value select filter interval
                 */
                this.selectedIntervalItems = [
                  {
                    id: "1_days",
                    "name": this.translate.instant('select.1_days'),
                    "plan": PlanLevel.FREE
                  }
                ];

                /**
                 End translate options select filter
                 **/

                /**
                 * Filter item by plan
                 */
                this.itemListInterval = enableByPlan(this.plan as any,this.itemListInterval)


                /**
                 * Call API based on default value
                 */
                this.getStats(this.selectedIntervalItems[0].id)
              }

            },
            (err) => {
              this.loading = false
            }
        );
    this.ref.detectChanges();

  }

  /**
   * Call APIs by selectValue
   */

  async getStats(selectValue: string) {

    let start_date: string = dayjs().format('YYYY-MM-DD');
    let end_date: string = dayjs().format('YYYY-MM-DD');
    let interval;

    switch (selectValue) {
      case '1_days':
        interval = "daily"
        end_date = dayjs().subtract(1, 'days').format('YYYY-MM-DD');
        break;
      case '7_days':
        interval = "daily"
        end_date = dayjs().subtract(7, 'days').format('YYYY-MM-DD');
        break;
      case '28_days':
        interval = "daily"
        end_date = dayjs().subtract(28, 'days').format('YYYY-MM-DD');
        break;
      case '12_months':
        interval = "monthly"
        end_date = dayjs().subtract(12, 'month').format('YYYY-MM-DD');
        break;
      case 'current_month':
        interval = "monthly"
        end_date = dayjs().date(1).subtract(0, 'month').format('YYYY-MM-DD');
        break;
      case 'previous_month':
        interval = "monthly"
        end_date = dayjs().subtract(1, 'month').date(1).format('YYYY-MM-DD');
        start_date = dayjs().subtract(1, 'month').endOf('month').format('YYYY-MM-DD');
        break;
      case 'current_year':
        interval = "yearly"
        end_date = dayjs().dayOfYear(1).subtract(0, 'year').format('YYYY-MM-DD');
        break;
      case 'last_year':
        interval = "yearly"
        end_date = dayjs().subtract(1, 'year').startOf('year').format('YYYY-MM-DD');
        start_date = dayjs().subtract(1, 'year').endOf('year').format('YYYY-MM-DD');
        break;
      default:
        return
    }


    forkJoin([
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'bookings'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'availability'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'applications_count'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'active_users'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'applications_and_users_by_weekday'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'applications_by_user'),
      this.apiService.getStats(interval, end_date, start_date, this.selectedItems, this.tenantId, 'applications-and-users-by-time-slot'),
    ]).pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            res => {
              this.loading = false
              this.bookings = res[0]
              this.availability = res[1]
             this.applications_count = res[2]
              this.active_users = res[3]
              this.chartsBarUser = res[4];
              this.applications_by_user = res[5];
              this.chartsBar_applications_and_users_by_time_slot = res[6];
              this.charts = {
                labels: ['Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
                datasets: [
                  {
                    data: [Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100)],
                    label: 'Saturazione',
                  }
                ],
                "plan": PlanLevel.FREE
              }
              this.chartsBarUser = {
                ...this.chartsBarUser,
                "plan": PlanLevel.FREE
              }
              this.chartsBar_applications_and_users_by_time_slot = {
              ...this.chartsBar_applications_and_users_by_time_slot,
                "plan": PlanLevel.FREE
              }
              this.chartsPie = {
                labels: ['Desktop', 'Mobile', 'Telefono'],
                datasets: [{
                  data: [Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100)]
                },
                ],
                "plan": PlanLevel.FREE
              }
              this.chartsPie1 = {
                labels: ['Rifiutate', 'Accettate con modifica', 'Test'],
                datasets: [{
                  data: [Math.round(Math.random() * 100), Math.round(Math.random() * 100), Math.round(Math.random() * 100)]
                }],
                "plan": PlanLevel.FREE
              }
            }
        )
  }


  onItemSelect() {
    this.getStats(this.selectedIntervalItems[0].id)
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  loadTab1() {
    setTimeout(() => {
      this.tab1 = true
      this.tab2 = false
    }, 500)
  }

  loadTab2() {
    setTimeout(() => {
      this.tab1 = false
      this.tab2 = true
    }, 500)

  }

  validateConfTenant(value){
    let p;
    try {
      p = JSON.parse(value);
      const values = Object.values(PlanLevel);
      const check = values.find(element => {
        return element === p.plan.toUpperCase();
      });
      if (check) {
        return p.plan.toUpperCase()
      } else {
        this.validate = false
        return 'FREE'
      }
    } catch (e) {
      this.validate = false
      return 'FREE'
    }

  }
}
