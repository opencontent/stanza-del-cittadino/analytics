import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-period-labels',
  templateUrl: './period-labels.component.html',
  styleUrls: ['./period-labels.component.scss']
})
export class PeriodLabelsComponent implements OnInit {

  @Input() data: any
  @Input() key: string

  constructor() { }

  ngOnInit(): void {}

}
