import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {BaseChartDirective} from "ng2-charts";
import {ChartConfiguration, ChartData, ChartType} from "chart.js";
import DataLabelsPlugin from "chartjs-plugin-datalabels";

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.scss']
})
export class PieComponent implements OnInit {

  @ViewChild(BaseChartDirective) pie: BaseChartDirective | any;
  @Input() data: any = null
  @Input() textLabel: string = ''
  @Input() demo: boolean = false

  // Pie
  pieChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    animation: {
      duration: 2000
    },
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
        position: 'top',
      },
      title: {
        display: true,
        text: this.textLabel,
        padding: {
          top: 20,
          bottom: 30
        }
      },
      datalabels: {
        formatter: (value, ctx) => {
          if (ctx.chart.data.labels) {
            return ctx.chart.data.labels[ctx.dataIndex];
          }
        },
      },
    }
  };
  pieChartData: ChartData<'pie'> = {datasets: [], labels: []}
  pieChartType: ChartType = 'pie';
  pieChartPlugins = [DataLabelsPlugin ];

  constructor() { }

  ngOnInit(): void {
    this.pieChartData = this.data
    this.pieChartOptions.plugins.title.text = this.textLabel
    if(this.demo){
      this.randomize()
    }else{
      this.pieChartData.datasets[0] = {
        ...this.pieChartData.datasets[0],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)'
        ],
        borderColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
        ],
        borderWidth: 1
      }
      this.pie?.update();
    }
  }

  randomize(): void {
    this.pieChartData.datasets[0].data = [
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100)
    ];
    this.pieChartData.datasets[0] = {
      ...this.pieChartData.datasets[0],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)'
      ],
      borderColor: [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
      ],
      borderWidth: 1
    }
    this.pie?.update();
  }

}
