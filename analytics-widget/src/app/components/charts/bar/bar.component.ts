import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ChartConfiguration, ChartData, ChartEvent, ChartType} from "chart.js";
import DataLabelsPlugin from "chartjs-plugin-datalabels";
import {BaseChartDirective} from "ng2-charts";
import {PlanLevel} from "../../../models/plan";

@Component({
    selector: 'app-bar',
    templateUrl: './bar.component.html',
    styleUrls: ['./bar.component.scss']
})
export class BarComponent implements OnInit, OnChanges {

    @ViewChild(BaseChartDirective) chart: BaseChartDirective | any;
    @Input() data: any = null
    @Input() textLabel: string = ''
    @Input() currentPlan = null

    barChartType: ChartType = 'bar';
    barChartPlugins = [
        DataLabelsPlugin
    ];
    barChartOptions: ChartConfiguration['options'] = {
        responsive: true,
        animation: {
            duration: 2000
        },
        // We use these empty structures as placeholders for dynamic theming.
        scales: {
            x: {},
            y: {
                min: 0
            }
        },
        plugins: {
            legend: {
                display: true,
                position: 'bottom',
            },
            datalabels: {
                anchor: 'end',
                align: 'end',
                display: true
            },
            title: {
                display: true,
                text: this.textLabel,
                padding: {
                    top: 20,
                    bottom: 30
                }
            }
        }
    };
    barChartData: ChartData<'bar'> = {datasets: [], labels: [] }
    backgroundPrimary = {
        backgroundColor: [
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)',
            'rgba(124,181,236, 0.5)'
        ],
        hoverBackgroundColor: [
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)'
        ],
        borderColor: [
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)'
        ],
        hoverBorderColor: [
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)',
            'rgb(124,181,236)'
        ],
        borderWidth: 1
    }
    backgroundSecondary = {
        backgroundColor: [
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)',
            'rgba(67,67,72, 0.5)'
        ],
        borderColor: [
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)'
        ],
        hoverBackgroundColor: [
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)'
        ],
        hoverBorderColor: [
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)',
            'rgb(67,67,72)'
        ],
        borderWidth: 1
    }
    plan: any;


    constructor() {}

    ngOnInit(): void {
    this.loadData()
    }

    chartClicked({event, active}: { event?: ChartEvent, active?: {}[] }): void {
        //  console.log(event, active);
    }

    chartHovered({event, active}: { event?: ChartEvent, active?: {}[] }): void {
        // console.log(event, active);
    }

    randomize(): void {
        this.barChartData.datasets[0].data = [
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100),
            Math.round(Math.random() * 100)
        ];
        this.barChartData.datasets[0] = {
            ...this.barChartData.datasets[0],
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(255, 159, 64, 0.5)',
                'rgba(255, 205, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(201, 203, 207, 0.5)'
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
            ],
            borderWidth: 1
        }
        this.chart?.update();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.loadData()
    }

    loadData(){
        this.barChartData = this.data
        this.loadPlan()
        this.barChartOptions.plugins.title.text = this.textLabel
        if (this.plan === 'DEMO') {
            this.randomize()
        } else {
            if (this.barChartData.datasets.length === 1) {
                this.barChartData.datasets[0] = {
                    ...this.barChartData.datasets[0],
                    ...this.backgroundPrimary
                }
            } else {
                this.barChartData.datasets[0] = {
                    ...this.barChartData.datasets[0],
                    ...this.backgroundPrimary
                }
                this.barChartData.datasets[1] = {
                    ...this.barChartData.datasets[1],
                    ...this.backgroundSecondary
                }
            }

            this.chart?.update();
        }
    }

    loadPlan(){
        if(this.currentPlan == 'SOON'){
            this.plan = 'SOON'
        }else if(this.currentPlan == 'PRO'){
            this.plan = 'SOON'
        } else if(this.currentPlan >= PlanLevel.FREE){
            this.plan = undefined
        }
    }
}
