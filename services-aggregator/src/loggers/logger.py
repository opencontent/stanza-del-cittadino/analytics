# Logger configuration
import logging
import os

from dotenv import load_dotenv

load_dotenv()

LOG_LEVEL = os.environ.get("LOG_LEVEL", "DEBUG")
LOG_FORMAT = os.environ.get("LOG_FORMAT", "%(asctime)s %(name)-32s %(levelname)-8s %(message)s")
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

clickhouse_logger = logging.getLogger("services-metrics-aggregator.clickhouse")
ksqldb_logger = logging.getLogger('services-metrics-aggregator.ksql')
main_logger = logging.getLogger('services-metrics-aggregator.main')
