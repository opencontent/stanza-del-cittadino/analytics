import json
from json import JSONEncoder

from pydantic import BaseModel
from datetime import date, datetime
from uuid import UUID


class AggregateObjEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime("%Y-%m-%d")
        elif isinstance(o, UUID):
            return str(o)
        return o.__dict__


class AggregateObj(BaseModel):
    day: datetime
    tenant_id: UUID
    service_id: UUID
    user_id: UUID
    applications_count: int
    week: int
    month: int
    year: int

    def json(self):
        return json.dumps(self, cls=AggregateObjEncoder)
