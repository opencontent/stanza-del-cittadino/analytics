import json

from src.controllers.ksqldb import KsqlDB
from src.controllers.clickhouse_client import ClickHouseClient
from src.models.AggregateObj import AggregateObj
from src.loggers.logger import main_logger as logger
from datetime import datetime, date, timedelta
import os
import asyncio

KSQLDB_SERVER = os.getenv('KSQLDB_SERVER', default='http://0.0.0.0:8088')
CLICKHOUSE_HOST = os.getenv('CLICKHOUSE_HOST', default='localhost')
CLICKHOUSE_USER = os.getenv('CLICKHOUSE_USER', default='')
CLICKHOUSE_SECRET = os.getenv('CLICKHOUSE_SECRET', default='')
CLICKHOUSE_DB = os.getenv('CLICKHOUSE_DB', default='sdc_analytics')


async def get_last_saved_date(table_name):
    try:
        async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
            last_saved_date = await client.get_last_saved_date(table_name)
        return last_saved_date[0][0] + timedelta(days=1)
    except Exception as e:
        logger.error(f"An error occurred: {e}")


async def get_applications_count():
    try:
        last_saved_date = (await get_last_saved_date('service_stats'))\
            .replace(hour=0, minute=0, second=0, microsecond=0)\
            .astimezone()\
            .isoformat()
        current_date = datetime.now()\
            .replace(hour=0, minute=0, second=0, microsecond=0)\
            .astimezone()\
            .isoformat()
        async with KsqlDB(KSQLDB_SERVER) as ksql_client:
            query = f"SELECT * " \
                    f"FROM APPLICATIONS_BY_DAY_TENANT_SERVICE_AND_USER " \
                    f"WHERE day BETWEEN CAST(\'{last_saved_date}\' AS TIMESTAMP) " \
                    f"AND CAST(\'{current_date}\' AS TIMESTAMP);"
            logger.debug(query)
            await ksql_client.query(query, save_aggregation)
    except Exception as e:
        logger.error(f"An error occurred: {e}")


async def save_aggregation(result, **kwargs):
    values = json.loads(result)
    values = values['row']['columns']
    obj_to_insert = AggregateObj(
        day=values[0],
        tenant_id=values[1],
        service_id=values[2],
        user_id=values[3],
        applications_count=values[4],
        week=int(datetime.strptime(values[0].split('T')[0], "%Y-%m-%d").strftime("%V")),
        month=int(datetime.strptime(values[0].split('T')[0], "%Y-%m-%d").strftime("%m")),
        year=int(datetime.strptime(values[0].split('T')[0], "%Y-%m-%d").strftime("%Y"))
    )
    table_name = 'service_stats'
    columns = ['day', 'tenant_id', 'service_id', 'user_id', 'applications_count', 'week', 'month', 'year']
    async with ClickHouseClient(CLICKHOUSE_HOST, CLICKHOUSE_USER, CLICKHOUSE_SECRET, CLICKHOUSE_DB) as client:
        await client.insert([obj_to_insert.dict()], table_name, columns)
    return result


if __name__ == '__main__':
    asyncio.run(get_applications_count())
