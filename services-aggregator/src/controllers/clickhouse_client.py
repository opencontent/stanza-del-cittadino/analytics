from aioch import Client
from src.loggers.logger import clickhouse_logger as logger


class ClickHouseClient:
    def __init__(self, host, user, secret, db):
        self._host = host
        self._user = user
        self._secret = secret
        self._db = db

    async def __aenter__(self):
        self.client = Client(
            self._host,
            # user=self._user,
            # password=self._secret,
            # secure=True,
            # verify=False,
            database=self._db
            # compression=True
        )
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        try:
            await self.client.disconnect()
        except Exception as e:
            logger.exception(f"An error occurred: {e}")

    async def get_most_recent_date(self, table_name, aggregation_param):
        try:
            result = await self.client.execute('SELECT MAX(day) as day, tenant_id, ' + aggregation_param + ' FROM ' +
                                               table_name + ' GROUP BY tenant_id, ' + aggregation_param)
            return result
        except Exception as e:
            logger.exception(f"An error occurred: {e}")

    async def get_last_saved_date(self, table_name):
        try:
            result = await self.client.execute('SELECT MAX(day) as day FROM ' + table_name)
            logger.debug(f"Most recent date: {result[0][0].strftime('%Y-%m-%d')}")
            return result
        except Exception as e:
            logger.exception(f"An error occurred: {e}")

    async def insert(self, aggregate_obj, table_name, columns):
        columns = await self._format_columns(columns)
        try:
            logger.debug(f"Inserting {aggregate_obj}")
            await self.client.execute('INSERT INTO ' + table_name + ' ' + columns + ' VALUES', aggregate_obj)
        except Exception as e:
            logger.exception(f"An error occurred: {e}")

    async def _format_columns(self, columns):
        table_columns = '('
        for i in range(len(columns)):
            if i < len(columns) - 1:
                table_columns = table_columns + columns[i] + ", "
            else:
                table_columns = table_columns + columns[i] + ")"
        return table_columns
