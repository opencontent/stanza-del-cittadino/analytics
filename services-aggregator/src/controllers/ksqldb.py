from ksql import KSQLAPI
from src.loggers.logger import ksqldb_logger as logger
import requests


class KsqlDB:
    def __init__(self, ksqldb_server):
        self.ksqlb_server = ksqldb_server

    async def __aenter__(self):
        try:
            self.client = KSQLAPI(url=self.ksqlb_server)
            return self
        except ValueError as ve:
            logger.critical(f'Error connecting to ksql, check the KSQLDB_SERVER '
                            f'environment variable and the ksqldb service status.\nError: {ve}')
        except requests.exceptions.ConnectionError as ce:
            logger.critical(f'Error connecting to ksql, check the KSQLDB_SERVER '
                            f'environment variable and the ksqldb service status.\nError: {ce}')

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        pass

    async def query(self, query, func, **kwargs):
        logger.info(f"Starting quering ksqldb at {self.ksqlb_server}...")
        results = self.client.query(query)
        next(results)
        try:
            for result in results:
                ''' The generator returns a strings representing a part of a list.
                The last call before the StopIteration->RuntimeError ends with ']' '''
                logger.debug(f'\n\nParsing result: {result}')
                result = result.strip()[:-1]
                await func(result, **kwargs)
        except RuntimeError as re:
            logger.debug(f"result not yet parsed.")
