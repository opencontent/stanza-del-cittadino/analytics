from kafka import KafkaProducer

import os
import json

KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS', default='localhost:29092')
KAFKA_TOPIC_MEETINGS = os.getenv('KAFKA_TOPIC_MEETINGS', default='meetings')
KAFKA_TOPIC_CALENDARS = os.getenv('KAFKA_TOPIC_CALENDARS', default='calendars')
KAFKA_TOPIC_APPLICATIONS = os.getenv('KAFKA_TOPIC_APPLICATIONS', default='applications')


def read_json(filename):
    with open(filename) as f:
        data = json.load(f)
    return data


def json_serializer(data):
    return json.dumps(data).encode("utf-8")


def produce_message(msg_path, topic,
                    producer=KafkaProducer(bootstrap_servers=[broker for broker in KAFKA_BOOTSTRAP_SERVERS.split(',')],
                                           value_serializer=json_serializer)):
    messages = read_json(msg_path)
    for message in messages:
        producer.send(topic, message).get(timeout=30)
        print(message)


if __name__ == "__main__":
    context_path = 'messages/'
    produce_message(context_path + 'meetings.json', KAFKA_TOPIC_MEETINGS)
    produce_message(context_path + 'calendars.json', KAFKA_TOPIC_CALENDARS)
    produce_message(context_path + 'applications.json', KAFKA_TOPIC_APPLICATIONS)
